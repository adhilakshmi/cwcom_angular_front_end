import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DlFocusListComponent } from './dl-focus-list.component';

describe('DlFocusListComponent', () => {
  let component: DlFocusListComponent;
  let fixture: ComponentFixture<DlFocusListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DlFocusListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DlFocusListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
