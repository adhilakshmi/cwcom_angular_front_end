import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileRepLibComponent } from './profile-rep-lib.component';

describe('ProfileRepLibComponent', () => {
  let component: ProfileRepLibComponent;
  let fixture: ComponentFixture<ProfileRepLibComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileRepLibComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileRepLibComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
