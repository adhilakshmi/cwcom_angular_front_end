import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { premDiscChartData } from '../../data/indices/prem-disc-chart';
import { Title } from '@angular/platform-browser';
import { environment } from '../../../environments/environment';
import { MatDialog } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-indices',
  templateUrl: './indices.component.html',
  styleUrls: ['./indices.component.scss']
})
export class IndicesComponent implements OnInit {
  @ViewChild("methodologyCDLIPopup", {static: false}) methodologyCDLIPopup: TemplateRef<any>;
  @ViewChild("methodologyCWBDCPopup", {static: false}) methodologyCWBDCPopup: TemplateRef<any>;
  @ViewChild("bloombergIndexPopup", {static: false}) bloombergIndexPopup: TemplateRef<any>;
  @ViewChild("emailReqPopup", {static: false}) emailReqPopup: TemplateRef<any>;
  premDiscChartOptions : any;
  premDiscChartData = premDiscChartData;
  isAreaChartLoaded : boolean = false;
  isIndexCharLoaded : boolean = true;
  elevationData : any[] = Array();
  elevationDataSub : any[] = Array();
  date : Date;
  emailReqForm: FormGroup;
  submitted : boolean = false;
  loading : boolean = false;
  environment = environment;
  selPopupType : string = '';

  constructor(private titleService: Title, public dialog: MatDialog,
    private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.titleService.setTitle(environment.pageTitle.indices +'-'+ environment.pageTitle.cliffwater);
    this.premDiscChartOptions = this.generatePremDiscChart(premDiscChartData);
    /****************Email Required Form Validation****************** */
    this.emailReqForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
    });
  }

  /*******************************Menthod to generate Premium/Discount Chart****************************/
  generatePremDiscChart(data) {
    for(var i=0;i<= data[0].x.length ; i++){
      if (i % 17 == 0 || i == 0 || i == data[0].x.length - 1) {
        this.elevationDataSub = Array();
        this.date = new Date(data[0].x[i]);
        this.elevationDataSub.push(Date.UTC(this.date.getFullYear(), this.date.getMonth(), this.date.getDate()), data[0].PremDis[i]);
        this.elevationData.push(this.elevationDataSub);
      }
    }
    this.isAreaChartLoaded = true;
    return {
      chart: {
        type: 'area',
        width: screen.width < 768 ? "320" : "600",
      },
      title: null,
      subtitle: null,
      legend: {},
      tooltip: {},
      xAxis: {
        type: 'datetime',
        dateTimeLabelFormats: {
            year: '%b \'%y'
        },
        gridLineWidth: 0,
        tickInterval: 30 * 24 * 3600 * 1000 * 40,
        tickWidth: 1,
        tickColor: "#000000",
        lineWidth: 1,
        lineColor: "#000000"
      },
      yAxis: {
        title: null,
        tickInterval: 10,
        gridLineWidth: 1,
        gridLineColor: "#dddddd",
        gridLineDashStyle: "dash",
        lineWidth: 1,
        lineColor: "#000000",
        tickWidth: 1,
        tickColor: "#000000"

    },
    credits: {
        enabled: false
    },
    plotOptions: {
        xAxis: {
            lineWidth: 1,
            lineColor: '#303030'
        },
        areaspline: {
            fillOpacity: 1
        }

    },
    series: [{
        name: '% Premium/Discount on NAV',
        data: this.elevationData,
        color: '#D6D6D6',
        lineColor: '#000000'

      }]
    }

  }

  /**********************************Method to Open CDLI - Methodology Popup***************************************/
  openCDLIMethodologyPopup(){
    this.dialog.open(this.methodologyCDLIPopup);
  }

  /**********************************Method to Open CWBDC - Methodology Popup***************************************/
  openCWBDCMethodologyPopup(){
    this.dialog.open(this.methodologyCWBDCPopup);
  }

  /**********************************Method to Open CWBDC & CDLI Index on Bloomberg Popup***************************************/
  openBloombergIndexPopup() {
    this.dialog.open(this.bloombergIndexPopup, {
      width: '55%',
      height: '70%'
    });
  }

  /**********************************Method to Open Email Required Popup***************************************/
  openEmailReqPopup(type){
    console.log(type)
    this.selPopupType = type;
    this.dialog.open(this.emailReqPopup);
  }

  /**********************************Method to Close Opened Popup***************************************/
  dialogClose() {
    this.emailReqForm.markAsPristine();
    this.emailReqForm.markAsUntouched();
    this.emailReqForm.updateValueAndValidity();
    this.dialog.closeAll();
   
  }

  // convenience getter for easy access to form fields
  get f() { return this.emailReqForm.controls; }

  /**********************************Method to Submit Email Required Form***************************************/
  emailReqFormSubmit() {
    this.submitted = true;
      // stop here if form is invalid
      if (this.emailReqForm.invalid) {
          return;
      }
      this.loading = true;
      if(this.selPopupType == 'cdliHistory') {
        window.open(
          "//storage.googleapis.com/cdli/CDLI-History.xls",
          '_blank' // <- This is what makes it open in a new window.
        );
      }
      else if(this.selPopupType == 'cwbdcHistory') {
        window.open(
          "//storage.googleapis.com/cdli/CWBDC-History.xls",
          '_blank' // <- This is what makes it open in a new window.
        );
      }
      else if(this.selPopupType == 'cwbdcConst') {
        window.open(
          "//storage.googleapis.com/cdli/CWBDC-IndexConstituents.xls",
          '_blank' // <- This is what makes it open in a new window.
        );
      }

      this.dialogClose();
      this.loading = false;
  }

}
