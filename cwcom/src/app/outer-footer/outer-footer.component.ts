import { Component, OnInit } from '@angular/core';
import { CommonService } from '../services/common.service';

@Component({
  selector: 'app-outer-footer',
  templateUrl: './outer-footer.component.html',
  styleUrls: ['./outer-footer.component.scss']
})
export class OuterFooterComponent implements OnInit {
  currentYear : number = 2020;

  constructor(private commonService : CommonService) { 
    this.currentYear = this.commonService.getCurrentYear();
  }

  ngOnInit() {
  }

}
