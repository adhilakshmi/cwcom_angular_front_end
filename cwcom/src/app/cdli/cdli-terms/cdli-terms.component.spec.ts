import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CdliTermsComponent } from './cdli-terms.component';

describe('CdliTermsComponent', () => {
  let component: CdliTermsComponent;
  let fixture: ComponentFixture<CdliTermsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CdliTermsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CdliTermsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
