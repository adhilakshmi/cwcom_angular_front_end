import { Component, OnInit, AfterViewInit} from '@angular/core';
import { Title } from '@angular/platform-browser';
import { environment } from '../../../environments/environment';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MaterialService } from '../../services/material.service';

@Component({
  selector: 'app-fund-contact',
  templateUrl: './fund-contact.component.html',
  styleUrls: ['./fund-contact.component.scss']
})
export class FundContactComponent implements OnInit, AfterViewInit {
  hfFundContactForm : FormGroup;
  paFundContactForm : FormGroup;
  dlFundContactForm : FormGroup;
  hfFormSubmitted = false;
  paFormSubmitted = false;
  dlFormSubmitted = false;
  loading = false;
  environment : any = environment;
  aumCurrency : any = environment.fundContact.aumCurrency;
  hfStrategyList : any = environment.fundContact.hfStrategyList;
  hfSubStrategyList : any = environment.fundContact.hfSubStrategyList;
  hfSelSubStrategyList : any = [];
  paAssetClassList : any = environment.fundContact.paAssetClass;
  paAllStrategiesList : any = environment.fundContact.paAllStrategiesList;
  paSelStrategiesList : any = [];
  paAllSubStrategiesList : any = environment.fundContact.paAllSubStrategiesList;
  paSelSubStrategiesList : any = [];
  paUsOrNonUsList : any = environment.fundContact.paUsOrNonUsList;
  paGeogFocusList : any = environment.fundContact.paGeogFocusList;
  paRegFocusList : any = environment.fundContact.paRegFocusList;
  paSecFocusList : any = environment.fundContact.paSecFocusList;

  constructor(private titleService: Title,
    private materialService: MaterialService,
    private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.titleService.setTitle(environment.pageTitle.fundContact +'-'+ environment.pageTitle.cliffwater);
    /****************HF - Fund Contact Form Validation****************** */
    this.hfFundContactForm = this.formBuilder.group({
      hfFirmName: ['', [Validators.required]],
      hfFundName: ['', [Validators.required]],
      hfAddress: [''],
      hfAum: [''],
      hfAumCurrency: [''],
      hfStrategySelect: [''],
      hfSubStrategySelect: [''],
      hfContactName: ['', [Validators.required]],
      hfContactNumber: ['', [Validators.required]],
      hfContactEmail: ['']
    });

     /****************PA - Fund Contact Form Validation****************** */
     this.paFundContactForm = this.formBuilder.group({
      paFirmName: ['', [Validators.required]],
      paFundName: ['', [Validators.required]],
      paAddress: [''],
      paAum: [''],
      paAumCurrency: [''],
      paAssetClass: [''],
      paStrategySelect: [''],
      paSubStrategySelect: [''],
      paUsOrNonUs: [''],
      paGeogFocus: [''],
      paRegFocus: [''],
      paSecFocus: [''],
      paSecDesc: [''],
      paFundSize: [''],
      paCapSize: [''],
      paFirstClose: [''],
      paFinalClose: [''],
      paHeadQuarters: [''],
      paOfficeLocation: [''],
      paPlacementAgent: [''],
      paFundPPMDoc: [''],
      paFundPPTDoc: [''],
      paContactName: ['', [Validators.required]],
      paContactNumber: ['', [Validators.required]],
      paContactEmail: ['']
    });

    /****************DL - Fund Contact Form Validation****************** */
    this.dlFundContactForm = this.formBuilder.group({
      dlFirmName: ['', [Validators.required]],
      dlFundName: ['', [Validators.required]],
      dlPerformanceEnding: [''],
      dlIncepDate: [''],
      dlGrossAssets: [''],
      dlNetAssets: [''],
      dlFundLvlLev: [''],
      dlGrossIRR: [''],
      dlNetIRR: [''],
      dlPercentDrawn: [''],
      dlPercentDist: [''],
      dlAvgCompLev: [''],
      dlAvgCompLevInvst: [''],
      dlAvgPortComp: [''],
      dlAvgSponsorBacked: [''],
      dlNumOfCredits: [''],
      dlPercentUs: [''],
      dlFstLien: [''],
      dlUnitranche: [''],
      dlSecLien: [''],
      dlSubDebts: [''],
      dlOthrEquity: ['']
    });
    
  }

  ngAfterViewInit() {
    //Init Select
    this.materialService.initSelect({});
  }

  // convenience getter for easy access to form fields
  get f() { return this.hfFundContactForm.controls; }

  /*******************************Method to submit HF - Fund Contact Form***************************************** */
  hfFundContactFormSubmit () {
    this.hfFormSubmitted = true;
    // stop here if form is invalid
    if (this.hfFundContactForm.invalid) {
        return;
    }
    this.loading = true;
  }

  // convenience getter for easy access to form fields
  get pa() { return this.paFundContactForm.controls; }

  /*******************************Method to submit PA - Fund Contact Form***************************************** */
  paFundContactFormSubmit () {
    this.paFormSubmitted = true;
    // stop here if form is invalid
    if (this.paFundContactForm.invalid) {
        return;
    }
    this.loading = true;
  }

     // convenience getter for easy access to form fields
  get dl() { return this.dlFundContactForm.controls; }

  /*******************************Method to submit PA - Fund Contact Form***************************************** */
  dlFundContactFormSubmit () {
    this.dlFormSubmitted = true;
    // stop here if form is invalid
    if (this.dlFundContactForm.invalid) {
        return;
    }
    this.loading = true;
  }

  /***************************Fund Contact - Tab Click Event************************************/
  tabClick(tab) {
    this.materialService.initSelect({});
  }

  /***************************HF - Sub-Strategy Value based on Selected Strategy************************************/
  hfStrategySelectChange(value: string) {
    this.hfSelSubStrategyList = this.hfSubStrategyList[value];
    setTimeout(()=>{
      this.materialService.initSelect({});
    }, 0);
  }

  /***************************PA - Strategy Value based on Selected Asset Class************************************/
  paAssetClassSelectChange(value: string) {
    this.paSelStrategiesList = this.paAllStrategiesList[value];
    setTimeout(()=>{
      this.materialService.initSelect({});
    }, 0);
  }

  /***************************HF - Sub-Strategy Value based on Selected Strategy************************************/
  paStrategySelectChange(value: string) {
   this.paSelSubStrategiesList = this.paAllSubStrategiesList[value];
    setTimeout(()=>{
      this.materialService.initSelect({});
    }, 0);
  }

}
