import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { environment } from '../../../environments/environment';
import { MatDialog } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-research',
  templateUrl: './research.component.html',
  styleUrls: ['./research.component.scss']
})
export class ResearchComponent implements OnInit {
  @ViewChild("emailReqPopup", {static: false}) emailReqPopup: TemplateRef<any>;
  emailReqForm: FormGroup;
  submitted : boolean = false;
  loading : boolean = false;
  docURL : string = '';
  docType : string = '';
  docTitle : string = '';

  constructor(private titleService: Title, 
    public dialog: MatDialog,
    private formBuilder: FormBuilder ) { }

  ngOnInit() {
    this.titleService.setTitle(environment.pageTitle.research +'-'+ environment.pageTitle.cliffwater);
    /****************Email Required Form Validation****************** */
    this.emailReqForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
    });
  }

  /**********************************Method to Open Email Required Popup***************************************/
  openEmailReqPopup(url:string, title:string, type:string){
    this.docURL = url;
    this.docTitle = title;
    this.docType = type;
    this.dialog.open(this.emailReqPopup);
  }

   // convenience getter for easy access to form fields
   get f() { return this.emailReqForm.controls; }

   /**********************************Method to Submit Email Required Form***************************************/
   emailReqFormSubmit() {
     this.submitted = true;
       // stop here if form is invalid
       if (this.emailReqForm.invalid) {
           return;
       }
       this.loading = true;
       if(this.docType == 'home') {
          window.open(
            "/Home/ReadFile?path=docs/home/" + this.docURL + "&title=" + this.docTitle,
            '_blank' // <- This is what makes it open in a new window.
          );
       }
       else if(this.docType == 'research') {
          window.open(
            "/Research/ReadFile?path=docs/" + this.docURL + "&title=" + this.docTitle,
            '_blank' // <- This is what makes it open in a new window.
          );
       }
       this.dialogClose();
       this.loading = false;
   }

  /**********************************Method to Close Opened Popup***************************************/
  dialogClose() {
    this.emailReqForm.markAsPristine();
    this.emailReqForm.markAsUntouched();
    this.emailReqForm.updateValueAndValidity();
    this.dialog.closeAll();
   
  }

}
