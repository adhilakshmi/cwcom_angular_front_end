export const dataName: any[] = [{
    name: 'US Stocks',
    y: 25,
    color: '#95B3D7'
},
{
    name: 'Non-US Stocks',
    y: 10,
    color: '#95B3D7'
},
{
    name: 'Emerging Markets',
    y: 5,
    color: '#95B3D7'
},
{
    name: 'Private Equity',
    y: 15,
    color: '#95B3D7'
},
{
    name: 'High Yield',
    y: 2,
    color: '#366090'
},
{
    name: 'Direct Lending',
    y: 10,
    color: '#366090'
},
{
    name: 'Opportunistic Credit ',
    y: 8,
    color: '#366090'
},
{
    name: 'Real Estate/REITs',
    y: 8,
    color: '#A6A6A6'
},
{
    name: 'Infrastructure',
    y: 5,
    color: '#A6A6A6'
},
{
    name:
        'Energy/MLPs ',
    y: 2,
    color: '#A6A6A6'
},
{
    name:
        'Core US Bonds',
    y: 8,
    color: '#7F7F7F'
}, {
    name:
        'Cash',
    y: 2,
    color: '#7F7F7F'
}];


export const dataAsset: any[] = [{
    name: "Equity",
    y: 55,
    color: '#95B3D7'
},
{
    name: "Credit",
    y: 20,
    color: '#366090'
},
{
    name: "Real Assets",
    y: 15,
    color: '#A6A6A6'
},
{
    name: "Rates",
    y: 10,
    color: '#7F7F7F'
}]