import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { invDDROuterChartData, invDDRInnerChartData } from '../../data/alternatives/inv-ddr-pie-chart';
import { oprDDROuterChartData, oprDDRInnerChartData } from '../../data/alternatives/opr-ddr-pie-chart';
import { legalDDROuterChartData, legalDDRInnerChartData } from '../../data/alternatives/legal-ddr-pie-chart';
import { Title } from '@angular/platform-browser';
import { environment } from '../../../environments/environment';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-alternatives',
  templateUrl: './alternatives.component.html',
  styleUrls: ['./alternatives.component.scss']
})
export class AlternativesComponent implements OnInit {
  invDDRPieChartOptions: any;
  invDDROuterChartData = invDDROuterChartData;
  invDDRInnerChartData = invDDRInnerChartData;
  oprDDRPieChartOptions: any;
  oprDDROuterChartData = oprDDROuterChartData;
  oprDDRInnerChartData = oprDDRInnerChartData;
  legalDDRPieChartOptions: any;
  legalDDROuterChartData = legalDDROuterChartData;
  legalDDRInnerChartData = legalDDRInnerChartData;
  @ViewChild("bdcsConfirmPopup", {static: false}) bdcsConfirmPopup: TemplateRef<any>;

  constructor(private titleService: Title, 
    public dialog: MatDialog) { }

  ngOnInit() {
    this.titleService.setTitle(environment.pageTitle.alternatives +'-'+ environment.pageTitle.cliffwater);
    this.invDDRPieChartOptions = this.generatePieChart(this.invDDROuterChartData, this.invDDRInnerChartData);
    this.oprDDRPieChartOptions = this.generatePieChart(this.oprDDROuterChartData, this.oprDDRInnerChartData);
    this.legalDDRPieChartOptions = this.generatePieChart(this.legalDDROuterChartData, this.legalDDRInnerChartData);
  }

  /**********************************Method to Generate Pie Charts***************************************/
  generatePieChart(outerData, innerData) {
    return {
        chart: {
          animation: {
              duration: 300
          },
          type: 'pie',
          width: screen.width < 768 ? "320" : "600",
          marginRight: 5,
        },
        tooltip: {},
        legend: {
          enabled: false,
          y: -10,
          symbolHeight: 12,
          itemDistance: 12,
          padding: 0,
          itemHoverStyle: {
              color: '#000000',
              cursor: 'default'
          },
          symbolWidth: 12,
          symbolRadius: 0,
          title: {
              text: ''
          }
      },
      title: {
          text: '',
          align: 'center',
          style: {
              fontSize: '20px',
              fontWeight: 'bold',
              border: '1px solid #379695'
          }
      },
      subtitle: {
        align: 'center',
        text: "",
        style: {
          fontSize: '16px',
          fontWeight: 'bold',
          color: '#000000'
        }
      },
      credits: {
        enabled: true
      },
      plotOptions: {
        series: {
            animation: false,
            point: {
                events: {
                    legendItemClick: function () {
                        return false;
                    }
                }
            }
        },
        pie: {
            dataLabels: {
                distance: 20,
                padding: 3,
                formatter: function () {
                    if (this.y > 0) return this.key;
                    else return null;
                },
                connectorPadding: 4,
                style: {
                    width: '120px',
                    fontSize: '12px',
                    color: '#000000',
                    fontWeight: 'normal'
                },
            },
        }
    },
    series: [{
        name: 'Category',
        size: screen.width < 768 ? '55%' : '75%',
        innerSize: screen.width < 768 ? '15%' : '55%',
        data: outerData
      },
      {
          name: 'Asset level',
          size: screen.width < 768 ? '25%' : '55%',
          showInLegend: true,
          dataLabels: {
              useHTML: true,
              enabled: screen.width < 768 ? false : true,
              style: {
                  width: '80px',
                  fontSize: '12px',
                  fontWeight: 'normal'
              },
              formatter: function () {
                  return '<span style="color:white;font-size:10px">' +
                      this.point.name + '</span>';
              },
              distance: -40
          },
          data: innerData,
      }]
    }
  }

  /**********************************Method to Open Email Required Popup***************************************/
  openBDCSConfirmMdl(){
    this.dialog.open(this.bdcsConfirmPopup, {
      width: '55%',
      height: '70%'
    });
  }

  /**********************************Method to Close Opened Popup***************************************/
  dialogClose()
  {
    this.dialog.closeAll();
  }

  bdcsConfirmBtnClick() {
    window.open(
      "https://www.amazon.com/Private-Debt-Opportunities-Corporate-Lending/dp/1119501156",
      '_blank' // <- This is what makes it open in a new window.
    );
    this.dialogClose();
  }

}
