import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CwbdcDefinitionsComponent } from './cwbdc-definitions.component';

describe('CwbdcDefinitionsComponent', () => {
  let component: CwbdcDefinitionsComponent;
  let fixture: ComponentFixture<CwbdcDefinitionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CwbdcDefinitionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CwbdcDefinitionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
