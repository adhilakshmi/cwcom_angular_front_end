import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CwbdcTermsComponent } from './cwbdc-terms.component';

describe('CwbdcTermsComponent', () => {
  let component: CwbdcTermsComponent;
  let fixture: ComponentFixture<CwbdcTermsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CwbdcTermsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CwbdcTermsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
