import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { environment } from '../../../../../environments/environment';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { CommonService } from '../../../../services/common.service';
import { MaterialService } from '../../../../services/material.service';
import { NouiFormatter } from "ng2-nouislider";
import {DecimalPipe} from '@angular/common';

@Component({
  selector: 'app-liquid-alt',
  templateUrl: './liquid-alt.component.html',
  styleUrls: ['./liquid-alt.component.scss'],
  providers:[DecimalPipe],
})
export class LiquidAltComponent implements OnInit, AfterViewInit {
  tooltips: [false,true];
  fundAssetsRange = [1, 99018];
  fundAssetsRangeConfig = {
    start: 0,
    connect: true,
    range: {
      min: 0,
      max: 99018
    },
    format: {
      from: Number,
      to: (value) => {                    
        return value;
      }
    },
    step: 1
  };
  expenseRatioRange = [17, 17257];
  expenseRatioRangeConfig = {
    start: 0,
    connect: true,
    range: {
      min: 17,
      max: 17257
    },
    format: {
      from: Number,
      to: (value) => {                    
        return value;
      }
    },
    step: 1
  };
  minInvRange = [0, 150000000];
  minInvRangeConfig = {
    start: 0,
    connect: true,
    range: {
      min: 0,
      max: 150000000
    },
    format: {
      from: Number,
      to: (value) => {                    
        return value;
      }
    },
    step: 1
  };

  constructor( private titleService: Title,
    private materialService: MaterialService,
    private decimalPipe: DecimalPipe,
    private commonService: CommonService ) { }

  ngOnInit() {
    this.titleService.setTitle(environment.pageTitle.hfLiquidAlt +'-'+ environment.pageTitle.cliffwater);
  }

  

  ngAfterViewInit() {
    //Init Select
    this.materialService.initSelect({});
    this.materialService.initCollapsible({});
  }


}
