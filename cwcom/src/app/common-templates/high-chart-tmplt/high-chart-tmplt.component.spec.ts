import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HighChartTmpltComponent } from './high-chart-tmplt.component';
import { HighchartsChartComponent } from 'highcharts-angular';
import * as Highcharts from 'highcharts';

describe('HighChartTmpltComponent', () => {
  let component: HighChartTmpltComponent;
  let fixture: ComponentFixture<HighChartTmpltComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HighChartTmpltComponent, HighchartsChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HighChartTmpltComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
