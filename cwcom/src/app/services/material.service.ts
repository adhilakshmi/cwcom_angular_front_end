import { Injectable } from '@angular/core';
import M from 'materialize-css';

@Injectable({
  providedIn: 'root'
})
export class MaterialService {

  constructor() { }

  /***********************Method to Init Materialize CSS Select**********************/
  initSelect(options) {
    M.FormSelect.init(document.querySelectorAll('select'), options);
  }

  /***********************Method to Init Materialize CSS Carousel**********************/
  initCarousel(options) {
    M.Carousel.init( document.querySelectorAll('.carousel'), options);
  }

  /***********************Method to Init Materialize CSS Scrollspy**********************/
  initScrollspy(options) {
    M.ScrollSpy.init( document.querySelectorAll('.scrollspy'), options);
  }

  /***********************Method to Init Materialize CSS Collapsible**********************/
  initCollapsible(options) {
    M.Collapsible.init( document.querySelectorAll('.collapsible'), options);
  }

}
