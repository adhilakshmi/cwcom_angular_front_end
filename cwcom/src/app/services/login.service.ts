import { Injectable } from '@angular/core';
import { contentHeaders } from '../common/headers';
import { CommonService } from './common.service';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private commonService : CommonService) { }

  /**
   * 
   *************************************API to Login****************************************** */
  login(username: string, password: string) {
    localStorage.setItem('id_token', username);
    this.commonService.setCurrentUserValue(username);
    //let url = environment.api_server + environment.apis.login;
    /*return this.http.post(url, data, httpOptions).
    .catch(this.commonService.handleError);*/
  }

  /**
   * 
   ***************************************API to Logout************************************** */
  logout() {
    // remove user from local storage to log user out
    localStorage.setItem('id_token', '');
    

   /* return this.http
    .post(`${this.url}/signout`, {}, {headers: contentHeaders})
    .toPromise()
    .then(response => {
      if(response.json().responseCode == 0) { //success
        localStorage.removeItem('id_token');
        localStorage.removeItem('userId');
        localStorage.removeItem('userName');
        window.location.href = '/Home';
      }
      return response.json();
    })
    .catch(this.commonService.handleError);*/
  }

}
