import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-cdli-terms',
  templateUrl: './cdli-terms.component.html',
  styleUrls: ['./cdli-terms.component.scss']
})
export class CdliTermsComponent implements OnInit {

  constructor(private titleService: Title) { }

  ngOnInit() {
    this.titleService.setTitle(environment.pageTitle.cdli+environment.pageTitle.terms +'-'+ environment.pageTitle.cliffwater);
  }

}
