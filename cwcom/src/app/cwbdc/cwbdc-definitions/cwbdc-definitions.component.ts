import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-cwbdc-definitions',
  templateUrl: './cwbdc-definitions.component.html',
  styleUrls: ['./cwbdc-definitions.component.scss']
})
export class CwbdcDefinitionsComponent implements OnInit {

  constructor(private titleService: Title) { }

  ngOnInit() {
    this.titleService.setTitle(environment.pageTitle.cwbdc+environment.pageTitle.definitions +'-'+ environment.pageTitle.cliffwater);
  }

}
