import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-cwbdc-terms',
  templateUrl: './cwbdc-terms.component.html',
  styleUrls: ['./cwbdc-terms.component.scss']
})
export class CwbdcTermsComponent implements OnInit {

  constructor(private titleService: Title) { }

  ngOnInit() {
    this.titleService.setTitle(environment.pageTitle.cwbdc+environment.pageTitle.terms +'-'+ environment.pageTitle.cliffwater);
  }

}
