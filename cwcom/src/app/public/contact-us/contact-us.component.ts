import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { environment } from '../../../environments/environment';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.scss']
})
export class ContactUsComponent implements OnInit {
  contactUsForm: FormGroup;
  submitted = false;
  loading = false;
  environment : any = environment;

  constructor(private titleService: Title,
    private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.titleService.setTitle(environment.pageTitle.contactUs +'-'+ environment.pageTitle.cliffwater);
    /****************Contact Us Form Validation****************** */
    this.contactUsForm = this.formBuilder.group({
      userName: ['', [Validators.required]],
      companyName: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      phoneNumber: [''],
      recaptchaReactive: [null, Validators.required],
      comments: ['', [Validators.required]]
    });
  }

  // convenience getter for easy access to form fields
  get f() { return this.contactUsForm.controls; }

   /*******************************Method to submit Contact Us Form***************************************** */
  contactUsFormSubmit() {
      this.submitted = true;
      // stop here if form is invalid
      if (this.contactUsForm.invalid) {
          return;
      }
      this.loading = true;
    }

}
