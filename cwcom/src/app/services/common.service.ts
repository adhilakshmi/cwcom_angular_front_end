import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  currDate = new Date();
  private currentUserSubject: BehaviorSubject<String>;
  public currentUser: Observable<String>;

  constructor() { }

  public onLimitChange(table, limit: any): void {
    table.limit = parseInt(limit, 10);
    table.recalculate();
    setTimeout(() => {
      if (table.bodyComponent.temp.length <= 0) {
        // TODO[Dmitry Teplov] test with server-side paging.
        table.offset = Math.floor((table.rowCount - 1) / table.limit);
      }
    });
  }

  /***************************************Method to Get Current Year********************************************************** */
  getCurrentYear(){
    return this.currDate.getFullYear()
  }

  /***************************************Method to Get Current Month********************************************************** */
  getCurrentMonth(){
    return this.currDate.toLocaleString('default', { month: 'long' });
  }

  /***************************************Method to Handle Errors********************************************************** */
  public handleError(error: any): Promise<any> {
    if(error.status == 403 || error.status == 401 || error.status==0) {//Logging out for access denied case
      localStorage.removeItem('id_token');
      localStorage.removeItem('userId');
      window.location.href = '/Account/Login';
    } else {
      return Promise.reject(error.message || error);
    }
  }

  /**************************Method to get whether the user is logged in or not************************************* */
  public getCurrentUserValue(): String {
   /* if(this.currentUserSubject) {
      return this.currentUserSubject.value;
    } else {
      return '';getMessage
    } */
    return localStorage.getItem('id_token'); //added for temporary check
  }

  /**************************Method to get whether the user is logged in or not************************************* */
  public setCurrentUserValue(id_token) {
    this.currentUserSubject = new BehaviorSubject<String>(id_token);
    this.currentUser = this.currentUserSubject.asObservable();
  }

}
