import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-cdli-definitions',
  templateUrl: './cdli-definitions.component.html',
  styleUrls: ['./cdli-definitions.component.scss']
})
export class CdliDefinitionsComponent implements OnInit {

  constructor(private titleService: Title) { }

  ngOnInit() {
    this.titleService.setTitle(environment.pageTitle.cdli+environment.pageTitle.definitions +'-'+ environment.pageTitle.cliffwater);
  }

}
