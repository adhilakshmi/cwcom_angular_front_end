import { Component, OnInit, AfterViewInit, HostListener, Inject  } from '@angular/core';
import { trigger, state, transition, style, animate } from '@angular/animations';  
import { Title } from '@angular/platform-browser';
import { environment } from '../../../environments/environment';
import { MaterialService } from '../../services/material.service';
import { DOCUMENT } from '@angular/common';
import { assetAllocationData, pEFundData, privateDebtData, realAssetsData, hedgeFundData, mediaLibraryData } from '../../data/research/research-data';


@Component({
  selector: 'app-research-pub',
  templateUrl: './research-pub.component.html',
  styleUrls: ['./research-pub.component.scss'],
  animations:[ 
    trigger('fade',
    [ 
      state('void', style({ opacity : 0})),
      transition(':enter',[ animate(300)]),
      transition(':leave',[ animate(500)]),
    ]
)]
})

export class ResearchPubComponent implements OnInit, AfterViewInit {
  assetAllocationData = assetAllocationData;
  assetAllocationDataRows = assetAllocationData.documents;
  pEFundData = pEFundData;
  pEFundDataRows = pEFundData.documents;
  privateDebtData = privateDebtData;
  privateDebtDataRows = privateDebtData.documents;
  realAssetsData = realAssetsData;
  realAssetsDataRows = realAssetsData.documents;
  hedgeFundData = hedgeFundData;
  hedgeFundDataRows = hedgeFundData.documents;
  mediaLibraryData = mediaLibraryData;
  mediaLibraryDataRows = mediaLibraryData.documents;

  
  constructor(private titleService: Title,
    @Inject(DOCUMENT) document,
    private materialService: MaterialService) { }

  ngOnInit() {
    this.titleService.setTitle(environment.pageTitle.research +'-'+ environment.pageTitle.cliffwater);
   // this.materialService.initScrollspy({});
  }

  ngAfterViewInit() {
    //Init Carousel
    this.materialService.initScrollspy({scrollOffset: 100});
  }

  /**
   * Method to set header fixed on scroll event
   * @param e 
   */
  @HostListener('window:scroll', ['$event'])
  onWindowScroll(e) {
     if (window.pageYOffset > 100) {
       let element = document.querySelectorAll('.researchSubNav');
       element[0].classList.add('fixed');
       element[0].classList.remove('default');
     } else {
      let element = document.querySelectorAll('.researchSubNav');
        element[0].classList.add('default');
        element[0].classList.remove('fixed'); 
     }
  }
}
