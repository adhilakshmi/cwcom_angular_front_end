// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  pageTitle : {
    cliffwater : ' Cliffwater LLC',
    aboutUs : 'About Us ',
    home : 'Home ',
    multiAsset : 'Multi-Asset ',
    alternatives : 'Alternatives ',
    research : 'Research ',
    indices : 'Indices ',
    contactUs : 'Contact Us ',
    fundContact : 'Fund Contact ',
    ourTeam : 'Our Team ',
    careers : 'Careers ',
    terms : 'Terms & Conditions ',
    cdli : 'CDLI ',
    cwbdc : 'CWBDC ',
    definitions : 'Definitions ',
    disclosures : 'Disclosures ',
    dashboard : 'Dashboard ',
    forgotPwd : 'Forgot Password ',
    hfLiquidAlt : 'HF - Liquid Alternatives ',
  },
  helpText : {
    emailReq : 'Please provide email address',
    emailInvalid : 'Please provide valid email address',
    userNameReq : 'Please provide user name',
    companyNameReq : 'Please provide company name',
    phoneNumberReq : 'Please provide phone number',
    phoneNumberInvalid : 'Please provide valid phone number',
    commentsReq : 'Please provide comments',
    firmNameReq : 'Please provide firm name',
    fundNameReq : 'Please provide fund name',
    contactNameReq : 'Please provide contact name',
    contactNumberReq : 'Please provide contact number',
    passwordReq : 'Please provide password'
  },
  fundContact : {
    aumCurrency : [
      {curr: 'AUD', name: 'AUD - Australia'},
      {curr: 'BRL', name: 'BRL - Brazil'},
      {curr: 'CAD', name: 'CAD - Canada'},
      {curr: 'CHF', name: 'CHF - Swiss franc'},
      {curr: 'CNY', name: 'CNY - China'},
      {curr: 'EUR', name: 'EUR - Euro'},
      {curr: 'GBP', name: 'GBP - British Pound'},
      {curr: 'HKD', name: 'HKD - Hong Kong'},
      {curr: 'INR', name: 'INR - India'},
      {curr: 'JPY', name: 'JPY - Japan'},
      {curr: 'KRW', name: 'KRW - South Korea'},
      {curr: 'RMB', name: 'RMB - Renminbi'},
      {curr: 'RUB', name: 'RUB - Russia'},
      {curr: 'SGD', name: 'SGD - Singapore'},
      {curr: 'USD', name: 'USD - U.S.'}
    ],
    hfStrategyList : ['Credit/Distressed','Equity Long/Short','Event Driven','Global Macro-Discretionary','Global Macro-Systematic','Market Neutral','Multi-Strategy'],
    hfSubStrategyList: 
    {
      'Credit/Distressed' : ['Distressed/Long Biased', 'Diversified Event Driven','Emerging Markets','Long/Short','Structured Credit'],
      'Equity Long/Short' : ['Long Biased Equity', 'Long/Short','Low Net Equity', 'Sector Focused','Short bias'],
      'Event Driven' : ['Activist', 'Diversified Event Driven','Long Biased Equity', 'Merger Arbitrage'],
      'Global Macro-Discretionary' : ['Commodities', 'Currencies','Diversified Discretionary Macro', 'Emerging Markets'],
      'Global Macro-Systematic' : ['Fundamental Systematic', 'Long Term','Medium Term', 'Short Term'],
      'Market Neutral' : ['Alternative Risk Premia', 'Convertible Arbitrage','Fixed Income Arbitrage', 'Quantitative Equity Market Neutral','Reinsurance','Statistical Arbitrage','Volatility Arbitrage'],
      'Multi-Strategy' : ['Multi-Strategy', 'Non-US','US/Global']
    },
    paAssetClass : ['Private Equity','Real Assets','Real Estate'],
    paAllStrategiesList : {
      'Private Equity' : ['Buyout','Debt','Fund of Funds','Secondaries','Venture Capital'],
      'Real Assets' : ['Agriculture','Energy','Infrastructure','Mining','Timber'],
      'Real Estate' : ['Core - RE','Core Plus - RE','Debt - RE','Opportunistic - RE','Real Estate','Value Added - RE']
    },
    paAllSubStrategiesList : {
      'Buyout' : ['Medium - Buyout - PE', 'Small - Buyout - PE', 'Large - Buyout - PE', 'Buyout - PE', 'Growth Equity - Buyout - PE'],
      'Debt' : ['Bank Loans - Debt - PE', 'Control Distressed - Debt - PE', 'Debt - PE', 'Direct Lending - Debt - PE', 'Mezzanine - Debt - PE','Non-Control Distressed - Debt - PE','Opportunistic Credit - Debt - PE','TALF - Debt - PE'],
      'Fund of Funds' : ['Fund of Funds - PE'],
      'Secondaries' : ['Secondaries - PE'],
      'Venture Capital' : ['Diverse - Venture Capital - PE', 'Early - Venture Capital - PE', 'Late - Venture Capital - PE', 'Venture Capital - PE'],
      'Agriculture' : ['Agribusiness - Agriculture - RA', 'Agriculture - RA', 'Debt - Agriculture - RA', 'Midstream - Agriculture - RA', 'Upstream - Agriculture - RA'],
      'Energy' : ['Debt - Energy - RA', 'Diversified - Energy - RA', 'Downstream - Energy - RA', 'Energy - RA', 'Midstream - Energy - RA','Oilfield Services - Energy - RA','Renewables - Energy - RA','Resources - Energy - RA','Upstream - Energy - RA'],
      'Infrastructure' : ['Core - Infrastructure - RA', 'Debt - Infrastructure - RA', 'Infrastructure - RA', 'Value Added - Infrastructure - RA'],
      'Mining' : ['Debt - Mining - RA', 'Equity - Mining - RA', 'Mining - RA'],
      'Timber' : ['Timber - RA'],
      'Core - RE' : ['Core - RE'],
      'Core Plus - RE' : ['Core Plus - RE'],
      'Debt - RE' : ['Debt - RE'],
      'Real Estate' : ['Real Estate'],
      'Value Added - RE' : ['Value Added - RE']
    },
    paUsOrNonUsList : ['Non-US','US'],
    paGeogFocusList : ['Asia','Emerging Markets','Europe','Global','US','Unspecified'],
    paRegFocusList : ['Africa','Asia','Eastern Europe','Latin America','North America','Oceania','Western Europe'],
    paSecFocusList : ['Agriculture','Communications/Media','Consumer','Diversified','Energy','Financial/Business Services','Healthcare','Hotel','Industrial','Information Technology','Infrastructure/Transportation','Land','Office','Other','Residential/Apt','Retail','Specialty']
  }

  
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
