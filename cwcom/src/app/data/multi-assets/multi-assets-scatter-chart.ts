export const returnRiskData : any[] = [
{
    'equityData' : [{"assetClass":"Equity","name":"US Stocks","return5":8.55,"vol":17,"y":8.55,"x":17},{"assetClass":"Equity","name":"Non-US Stocks","return5":8.72,"vol":18,"y":8.72,"x":18},{"assetClass":"Equity","name":"Emerging Markets","return5":10.98,"vol":26,"y":10.98,"x":26},{"assetClass":"Equity","name":"Diversified Private Equity","return5":12.1,"vol":20,"y":12.1,"x":20},{"assetClass":"Equity","name":"Venture","return5":15.5,"vol":30,"y":15.5,"x":30},{"assetClass":"Equity","name":"Global Equity","return5":8.97,"vol":17.7,"y":8.97,"x":17.7}],
    'eqColor' : '#95B3D7',
    'creditData' : [{"assetClass":"Credit","name":"Direct Lending (UL)","return5":7.48,"vol":6,"y":7.48,"x":6},{"assetClass":"Credit","name":"Direct Lending (1x Levered)","return5":10.12,"vol":12,"y":10.12,"x":12},{"assetClass":"Credit","name":"High Yield Bonds","return5":6.51,"vol":11,"y":6.51,"x":11},{"assetClass":"Credit","name":"IG Bonds","return5":4.43,"vol":6,"y":4.43,"x":6},{"assetClass":"Credit","name":"Opportunistic Credit","return5":7.51,"vol":15,"y":7.51,"x":15}],
    'creditColor' : '#366090',
    'realAssetsData' : [{"assetClass":"Real Assets","name":"Real Estate","return5":7.13,"vol":15,"y":7.13,"x":15},{"assetClass":"Real Assets","name":"REITs","return5":8.27,"vol":22,"y":8.27,"x":22},{"assetClass":"Real Assets","name":"MLPs","return5":8.5,"vol":17,"y":8.5,"x":17},{"assetClass":"Real Assets","name":"Infrastructure","return5":7.72,"vol":12,"y":7.72,"x":12},{"assetClass":"Real Assets","name":"Energy/Renewables","return5":14.66,"vol":31,"y":14.66,"x":31}],
    'realAssetsColor' : '#A6A6A6',
    'ratesData' : [{"assetClass":"Rates","name":"Core US Bonds","return5":3.38,"vol":4,"y":3.38,"x":4},{"assetClass":"Rates","name":"10-yr Treasury","return5":3.02,"vol":8,"y":3.02,"x":8},{"assetClass":"Rates","name":"LIBOR","return5":2.32,"vol":2,"y":2.32,"x":2}],
    'ratesColor' : '#7F7F7F'

}]