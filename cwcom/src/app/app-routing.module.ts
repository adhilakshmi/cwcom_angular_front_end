import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './account/login/login.component';
import { ForgotPasswordComponent } from './account/forgot-password/forgot-password.component';
import { HomeComponent } from './public/home/home.component';
import { AboutUsComponent } from './public/about-us/about-us.component';
import { MultiAssetComponent } from './public/multi-asset/multi-asset.component';
import { AlternativesComponent } from './public/alternatives/alternatives.component';
import { ResearchComponent } from './public/research/research.component';
import { IndicesComponent } from './public/indices/indices.component';
import { ContactUsComponent } from './public/contact-us/contact-us.component';
import { OurTeamComponent } from './public/our-team/our-team.component';
import { CareersComponent } from './public/careers/careers.component';
import { TermsComponent } from './public/terms/terms.component';
import { CdliDefinitionsComponent } from './cdli/cdli-definitions/cdli-definitions.component';
import { CdliDisclosuresComponent } from './cdli/cdli-disclosures/cdli-disclosures.component';
import { CdliTermsComponent } from './cdli/cdli-terms/cdli-terms.component';
import { CwbdcDefinitionsComponent } from './cwbdc/cwbdc-definitions/cwbdc-definitions.component';
import { CwbdcDisclosuresComponent } from './cwbdc/cwbdc-disclosures/cwbdc-disclosures.component';
import { CwbdcTermsComponent } from './cwbdc/cwbdc-terms/cwbdc-terms.component';
import { FundContactComponent } from './public/fund-contact/fund-contact.component';
import { DashboardComponent } from './private/dashboard/dashboard.component';
import { ResearchPubComponent } from './private/research-pub/research-pub.component';
import { AuthGuard } from './common/auth.guard';
import { ManagerResearchComponent } from './private/manager-library/hf/manager-research/manager-research.component';
import { LiquidAltComponent } from './private/manager-library/hf/liquid-alt/liquid-alt.component';
import { ProfileRepLibComponent } from './private/manager-library/pa/profile-rep-lib/profile-rep-lib.component';
import { ResearchActivityComponent } from './private/manager-library/pa/research-activity/research-activity.component';
import { DlFocusListComponent } from './private/manager-library/dl/dl-focus-list/dl-focus-list.component';
import { PaFocusListComponent } from './private/manager-library/pa/pa-focus-list/pa-focus-list.component';
import { HfFocusListComponent } from './private/manager-library/hf/hf-focus-list/hf-focus-list.component';

const routes: Routes = [
  {path: 'Account/Login', component: LoginComponent},
  {path: 'Home', component: HomeComponent},
  {path: 'AboutUs', component: AboutUsComponent},
  {path: 'MultiAsset', component: MultiAssetComponent},
  {path: 'Alternatives', component: AlternativesComponent},
  {path: 'ResearchPage', component: ResearchComponent},
  {path: 'Indices', component: IndicesComponent},
  {path: 'Contact', component: ContactUsComponent},
  {path: 'Contact/FundContact', component: FundContactComponent},
  {path: 'OurTeam', component: OurTeamComponent},
  {path: 'Careers', component: CareersComponent},
  {path: 'Terms', component: TermsComponent},
  {path: 'CDLI/Definitions', component: CdliDefinitionsComponent},
  {path: 'CDLI/Disclosures', component: CdliDisclosuresComponent},
  {path: 'CDLI/Terms', component: CdliTermsComponent},
  {path: 'CWBDC/Definitions', component: CwbdcDefinitionsComponent},
  {path: 'CWBDC/Disclosures', component: CwbdcDisclosuresComponent},
  {path: 'CWBDC/Terms', component: CwbdcTermsComponent},
  {path: 'Account/ForgotPassword', component: ForgotPasswordComponent},
  {path: 'Dashboard', component: DashboardComponent, canActivate: [AuthGuard]},
  {path: 'Research', component: ResearchPubComponent, canActivate: [AuthGuard]},
  {path: 'ManagerLibrary/FocusListFund', component: HfFocusListComponent, canActivate: [AuthGuard]},
  {path: 'ManagerLibrary/ManagerResearch', component: ManagerResearchComponent, canActivate: [AuthGuard]},
  {path: 'ManagerLibrary/LiquidAlt', component: LiquidAltComponent, canActivate: [AuthGuard]},
  {path: 'ManagerLibrary/FocusList', component: PaFocusListComponent, canActivate: [AuthGuard]},
  {path: 'ManagerLibrary/ProfileRptLib', component: ProfileRepLibComponent, canActivate: [AuthGuard]},
  {path: 'ManagerLibrary/PAResearchActivity', component: ResearchActivityComponent, canActivate: [AuthGuard]},
  {path: 'ManagerLibrary/DirectLendingFocusList', component: DlFocusListComponent, canActivate: [AuthGuard]},
  { path: '', redirectTo: 'Home', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
