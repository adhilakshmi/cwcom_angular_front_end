import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CdliDefinitionsComponent } from './cdli-definitions.component';

describe('CdliDefinitionsComponent', () => {
  let component: CdliDefinitionsComponent;
  let fixture: ComponentFixture<CdliDefinitionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CdliDefinitionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CdliDefinitionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
