import { Component, OnInit, AfterViewInit  } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { environment } from '../../../environments/environment';
import { teamMembers } from '../../data/our-team/team-member-details';
import { MaterialService } from '../../services/material.service';

@Component({
  selector: 'app-our-team',
  templateUrl: './our-team.component.html',
  styleUrls: ['./our-team.component.scss']
})
export class OurTeamComponent implements OnInit, AfterViewInit  {
  options = { fullWidth: false };
  teamNameArr : any[];
  teamMemberNameArr : any[] = [];
  teamMembersArr : any[] = teamMembers;
  showTeamMember : boolean = false;
  prevSelTeamMember : string = "";
  isTabChanged : boolean = false;

  constructor(private titleService: Title,
    private materialService: MaterialService) { }

  ngOnInit() {
    this.titleService.setTitle(environment.pageTitle.ourTeam +'-'+ environment.pageTitle.cliffwater);
    this.teamNameArr = [...new Set(teamMembers.map(item => item.team))];
    //Array for First Member of each team to init while tab click event
    for(var j=0;j< this.teamNameArr.length;j++){
      for(var i=0;i< teamMembers.length;i++){
        if(this.teamNameArr[j] == teamMembers[i].team) {
          this.teamMemberNameArr.push(this.teamMembersArr[i].name);
          break;
        }
      }
    }
  }

  ngAfterViewInit() {
    //Init Carousel
    this.materialService.initCarousel(this.options);
    this.initMemberDetail(0);
  }

  /***************************Our Team - Tab Click Event************************************/
  tabClick(tab) {
    this.materialService.initCarousel(this.options);
    this.initMemberDetail(tab.index);
  }

  /***************************Carousel Image Click Event************************************/
  carouselClik(teamMember) {
    var prevMemDom = document.getElementById(this.prevSelTeamMember);
    if(this.prevSelTeamMember && prevMemDom) {
      prevMemDom.classList.add("hide");
    }
    document.getElementById(teamMember).classList.remove("hide");
    this.prevSelTeamMember = teamMember;
  }

  /***************************Display Team Member Detail on Tab Init**************************/
  initMemberDetail(selTab : number = 0) {
    var selTabDOM = document.getElementById(this.teamMemberNameArr[selTab]);
    if(selTabDOM) {
      selTabDOM.classList.remove("hide");
      this.prevSelTeamMember = this.teamMemberNameArr[selTab];
    }
  }
  

}
