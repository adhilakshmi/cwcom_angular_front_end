import { Component, OnInit, AfterViewInit, OnDestroy  } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, AfterViewInit, OnDestroy {

  constructor(private titleService: Title) { }

  ngOnInit() {
    this.titleService.setTitle(environment.pageTitle.home +'-'+ environment.pageTitle.cliffwater);
  }

  ngAfterViewInit() {
    document.querySelector('body').classList.add('bgGray');
  }

  ngOnDestroy(): void {
    document.querySelector('body').classList.remove('bgGray');
  }

}
