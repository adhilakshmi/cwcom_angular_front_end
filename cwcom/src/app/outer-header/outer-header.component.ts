import { Component, OnInit, ViewChild, TemplateRef,Output, EventEmitter } from '@angular/core';
import { trigger, transition, animate, style } from '@angular/animations';
import { environment } from '../../environments/environment';
import { MatDialog } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoginService} from '../services/login.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-outer-header',
  templateUrl: './outer-header.component.html',
  styleUrls: ['./outer-header.component.scss'],
  animations: [
    trigger('slideInOut', [
      transition(':enter', [
        style({transform: 'translateY(-100%)'}),
        animate('0.35s 75ms ease-in-out', style({transform: 'translateY(0%)'}))
      ]),
      transition(':leave', [
        animate('0.35s 75ms ease-in-out', style({transform: 'translateY(-100%)'}))
      ])
    ])
  ]
})

export class OuterHeaderComponent implements OnInit {
  visible: boolean = false;
  @ViewChild("loginPopup", {static: false}) loginPopup: TemplateRef<any>;
  loginForm : FormGroup;
  submitted : boolean = false;
  loading : boolean = false;
  environment = environment;
 // @Output() userDetail = new EventEmitter<string>();

  constructor( public dialog: MatDialog,
    private formBuilder: FormBuilder,
    private router: Router,
    private loginService : LoginService) { }

  ngOnInit() {
    /****************Email Required Form Validation****************** */
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]],
      showPassword: ['']
    });
  }

  onSideNavClick(event) {
    this.visible = false;
  }

  /*****************Click event to toggle side nav******************/
  slideToggle() {
    this.visible = !this.visible;
  }

  /**********************************Method to Submit User Login Form***************************************/
  loginFormSubmit() {
    this.submitted = true;
      // stop here if form is invalid
      if (this.loginForm.invalid) {
          return;
      }
      this.loading = true;
      this.loginService.login(this.f.email.value, this.f.password.value);
     // this.userDetail.emit(this.f.email.value);
      this.router.navigate(['/Dashboard']);
      this.dialogClose();
      this.loading = false;
  }

  /**********************************Method to Open User Login Popup***************************************/
  openLoginPopup(){
    /****************Login Form Validation****************** */
    this.loginForm.reset();
    this.dialog.open(this.loginPopup);
  }

  /**********************************Click event for Forgot Password***************************************/
  forgotPwdClick() {
    this.dialogClose();
   // this.userDetail.emit('account');
  }

  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }

  /**********************************Method to Close Opened Popup***************************************/
  dialogClose()
  {
    this.dialog.closeAll();
  }
}
