import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagerResearchComponent } from './manager-research.component';

describe('ManagerResearchComponent', () => {
  let component: ManagerResearchComponent;
  let fixture: ComponentFixture<ManagerResearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManagerResearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagerResearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
