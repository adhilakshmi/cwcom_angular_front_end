import { HttpHeaders }    from '@angular/common/http';

export const contentHeaders = new HttpHeaders();
contentHeaders.append('content-type','application/json');
contentHeaders.append('accept','application/json');
contentHeaders.append('Cache-Control','no-cache');