import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResearchComponent } from './research.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material';

describe('ResearchComponent', () => {
  let component: ResearchComponent;
  let fixture: ComponentFixture<ResearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        MatDialogModule
      ],
      declarations: [ ResearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
