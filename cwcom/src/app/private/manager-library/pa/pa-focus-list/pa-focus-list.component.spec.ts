import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaFocusListComponent } from './pa-focus-list.component';

describe('PaFocusListComponent', () => {
  let component: PaFocusListComponent;
  let fixture: ComponentFixture<PaFocusListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaFocusListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaFocusListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
