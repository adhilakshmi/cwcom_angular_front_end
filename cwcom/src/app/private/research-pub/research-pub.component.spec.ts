import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResearchPubComponent } from './research-pub.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgxExtendedPdfViewerModule } from 'ngx-extended-pdf-viewer';

describe('ResearchPubComponent', () => {
  let component: ResearchPubComponent;
  let fixture: ComponentFixture<ResearchPubComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        NgxDatatableModule,
        NgxExtendedPdfViewerModule
      ],
      declarations: [ ResearchPubComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResearchPubComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
