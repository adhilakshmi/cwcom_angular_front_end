import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HfFocusListComponent } from './hf-focus-list.component';

describe('HfFocusListComponent', () => {
  let component: HfFocusListComponent;
  let fixture: ComponentFixture<HfFocusListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HfFocusListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HfFocusListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
