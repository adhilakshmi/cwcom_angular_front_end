import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { environment } from '../../../environments/environment';
import { Title } from '@angular/platform-browser';
import { LoginService} from '../../services/login.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm : FormGroup;
  submitted : boolean = false;
  loading : boolean = false;
  environment = environment;
  constructor(private formBuilder: FormBuilder,
    private router: Router,
    private loginService : LoginService,
    private titleService: Title) { }

    ngOnInit() {
      this.titleService.setTitle(environment.pageTitle.forgotPwd +'-'+ environment.pageTitle.cliffwater);
      /****************Forgot Password Form Validation****************** */
      this.loginForm = this.formBuilder.group({
        email: ['', [Validators.required, Validators.email]],
        password: ['', [Validators.required]],
        showPassword: ['']
      });
    }
  
   /* ngAfterViewInit() {
      document.querySelector('body').classList.add('accountPage');
    }
  
    ngOnDestroy(): void {
      document.querySelector('body').classList.remove('accountPage');
    } */
  
    // convenience getter for easy access to form fields
    get f() { return this.loginForm.controls; }
  
    /**********************************Method to Submit Forgot Password Form***************************************/
    loginFormSubmit() {
      this.submitted = true;
      // stop here if form is invalid
      if (this.loginForm.invalid) {
          return;
      }
      this.loading = true;
      this.loginService.login(this.f.email.value, this.f.password.value);
      //this.userDetail.emit(this.f.email.value);
      this.router.navigate(['/Dashboard']);
      this.loading = false;
    }

}
