import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CdliDisclosuresComponent } from './cdli-disclosures.component';

describe('CdliDisclosuresComponent', () => {
  let component: CdliDisclosuresComponent;
  let fixture: ComponentFixture<CdliDisclosuresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CdliDisclosuresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CdliDisclosuresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
