import { BrowserModule, Title  } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule }  from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RecaptchaModule, RECAPTCHA_SETTINGS, RecaptchaSettings } from 'ng-recaptcha';
import { NgxExtendedPdfViewerModule } from 'ngx-extended-pdf-viewer';
import { RecaptchaFormsModule } from 'ng-recaptcha/forms';
import { AppRoutingModule } from './app-routing.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { AppComponent } from './app.component';
import { InnerHeaderComponent } from './inner-header/inner-header.component';
import { InnerFooterComponent } from './inner-footer/inner-footer.component';
import { OuterHeaderComponent } from './outer-header/outer-header.component';
import { OuterFooterComponent } from './outer-footer/outer-footer.component';
import { LoginComponent } from './account/login/login.component';
import { HomeComponent } from './public/home/home.component';
import { AboutUsComponent } from './public/about-us/about-us.component';
import { MultiAssetComponent } from './public/multi-asset/multi-asset.component';
import { AlternativesComponent } from './public/alternatives/alternatives.component';
import { ResearchComponent } from './public/research/research.component';
import { IndicesComponent } from './public/indices/indices.component';
import { ContactUsComponent } from './public/contact-us/contact-us.component';
import { OurTeamComponent } from './public/our-team/our-team.component';
import { CareersComponent } from './public/careers/careers.component';
import { TermsComponent } from './public/terms/terms.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NouisliderModule } from 'ng2-nouislider';

import { MaterialModule } from './material.module';
import { MatIconModule, MatIconRegistry } from '@angular/material';
import { HighchartsChartComponent } from 'highcharts-angular';
import { HighChartTmpltComponent } from './common-templates/high-chart-tmplt/high-chart-tmplt.component';
import { CdliDefinitionsComponent } from './cdli/cdli-definitions/cdli-definitions.component';
import { CdliDisclosuresComponent } from './cdli/cdli-disclosures/cdli-disclosures.component';
import { CdliTermsComponent } from './cdli/cdli-terms/cdli-terms.component';
import { CwbdcDefinitionsComponent } from './cwbdc/cwbdc-definitions/cwbdc-definitions.component';
import { CwbdcDisclosuresComponent } from './cwbdc/cwbdc-disclosures/cwbdc-disclosures.component';
import { CwbdcTermsComponent } from './cwbdc/cwbdc-terms/cwbdc-terms.component';
import { FundContactComponent } from './public/fund-contact/fund-contact.component';
import { DashboardComponent } from './private/dashboard/dashboard.component';

import {CommonService} from './services/common.service';
import {LoginService} from './services/login.service';
import { AuthGuard } from './common/auth.guard';
import { ForgotPasswordComponent } from './account/forgot-password/forgot-password.component';
import { ResearchPubComponent } from './private/research-pub/research-pub.component';
import { ManagerResearchComponent } from './private/manager-library/hf/manager-research/manager-research.component';
import { LiquidAltComponent } from './private/manager-library/hf/liquid-alt/liquid-alt.component';
import { ProfileRepLibComponent } from './private/manager-library/pa/profile-rep-lib/profile-rep-lib.component';
import { ResearchActivityComponent } from './private/manager-library/pa/research-activity/research-activity.component';
import { DlFocusListComponent } from './private/manager-library/dl/dl-focus-list/dl-focus-list.component';
import { PaFocusListComponent } from './private/manager-library/pa/pa-focus-list/pa-focus-list.component';
import { HfFocusListComponent } from './private/manager-library/hf/hf-focus-list/hf-focus-list.component';

@NgModule({
  declarations: [
    AppComponent,
    InnerHeaderComponent,
    InnerFooterComponent,
    OuterHeaderComponent,
    OuterFooterComponent,
    LoginComponent,
    HighchartsChartComponent,
    HighChartTmpltComponent,
    HomeComponent,
    AboutUsComponent,
    MultiAssetComponent,
    AlternativesComponent,
    ResearchComponent,
    IndicesComponent,
    ContactUsComponent,
    OurTeamComponent,
    CareersComponent,
    TermsComponent,
    CdliDefinitionsComponent,
    CdliDisclosuresComponent,
    CdliTermsComponent,
    CwbdcDefinitionsComponent,
    CwbdcDisclosuresComponent,
    CwbdcTermsComponent,
    FundContactComponent,
    DashboardComponent,
    ForgotPasswordComponent,
    ResearchPubComponent,
    ManagerResearchComponent,
    LiquidAltComponent,
    ProfileRepLibComponent,
    ResearchActivityComponent,
    DlFocusListComponent,
    PaFocusListComponent,
    HfFocusListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    MatIconModule,
    FormsModule,
    ReactiveFormsModule,
    RecaptchaModule,
    RecaptchaFormsModule,
    HttpClientModule,
    NgxDatatableModule,
    NgxExtendedPdfViewerModule,
    NouisliderModule
  ],
  providers: [
    Title, 
    MatIconRegistry,
    CommonService, 
    LoginService,
    AuthGuard,
    {
      provide: RECAPTCHA_SETTINGS,
      useValue: {
        siteKey: '6LdK41AUAAAAAAzL3NRTi_miSWNbIHRpUBJFDYig',
      } as RecaptchaSettings,
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(
    public matIconRegistry: MatIconRegistry) {
    matIconRegistry.registerFontClassAlias('fontawesome', 'fa');
}
 }
