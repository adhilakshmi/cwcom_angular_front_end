import { Component, OnInit, Input } from '@angular/core';
import * as Highcharts from 'highcharts';

@Component({
  selector: 'app-high-chart-tmplt',
  templateUrl: './high-chart-tmplt.component.html',
  styleUrls: ['./high-chart-tmplt.component.scss']
})
export class HighChartTmpltComponent implements OnInit {
  @Input() plotChartOptions: any;
  highcharts: any;
  chartOptions: any;

  constructor() { }

  ngOnInit() {
    this.highcharts = Highcharts;

    this.chartOptions = { 
      chart: this.plotChartOptions.chart,
      title: this.plotChartOptions.title,
      subtitle: this.plotChartOptions.subtitle,
      xAxis: this.plotChartOptions.xAxis,
      yAxis: this.plotChartOptions.yAxis,
      credits: this.plotChartOptions.credits,
      plotOptions: this.plotChartOptions.plotOptions,
      series: this.plotChartOptions.series,
      tooltip: this.plotChartOptions.tooltip,
      legend: this.plotChartOptions.legend
    }

  }

}
