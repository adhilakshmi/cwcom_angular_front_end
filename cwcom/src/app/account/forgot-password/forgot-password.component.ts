import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { environment } from '../../../environments/environment';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {
  forgotPwdForm : FormGroup;
  submitted : boolean = false;
  loading : boolean = false;
  environment = environment;
  constructor(private formBuilder: FormBuilder,
    private titleService: Title) { }

  ngOnInit() {
    this.titleService.setTitle(environment.pageTitle.forgotPwd +'-'+ environment.pageTitle.cliffwater);
    /****************Forgot Password Form Validation****************** */
    this.forgotPwdForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]]
    });
  }

  /*ngAfterViewInit() {
    document.querySelector('body').classList.add('accountPage');
  }

  ngOnDestroy(): void {
    document.querySelector('body').classList.remove('accountPage');
  }*/

  // convenience getter for easy access to form fields
  get f() { return this.forgotPwdForm.controls; }

  /**********************************Method to Submit Forgot Password Form***************************************/
  forgotPwdFormSubmit() {
    this.submitted = true;
      // stop here if form is invalid
      if (this.forgotPwdForm.invalid) {
          return;
      }
      this.loading = true;
      this.loading = false;
  }

}
