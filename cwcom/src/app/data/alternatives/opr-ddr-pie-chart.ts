export const oprDDROuterChartData = [
    {
        name: 'Legal & Regulatory',
        y: 8.3,
        color: '#336799'
    },
    {
        name: 'Compliance',
        y: 8.3,
        color: '#86A7D2'
    },
    {
        name: 'Business Risk Management',
        y: 8.3,
        color: '#DCE6F2'
    },
    {
        name: 'Personnel',
        y: 6.3,
        color: '#C7D9F1'
    },
    {
        name: 'Segregation of Duties & Internal Controls',
        y: 6.3,
        color: '#B9CDE5'
    },
    {
        name: 'Service Providers',
        y: 6.3,
        color: '#95B3D9'
    },
    {
        name: 'Technology & Systems',
        y: 6.3,
        color: '#95B3D9'
    },
    {
        name: 'Trading',
        y: 6.3,
        color: '#969696'
    },
    {
        name: 'Portfolio Financing',
        y: 6.3,
        color: '#BFBFBF'
    },
    {
        name: 'Transparency',
        y: 6.3,
        color: '#4D4D4D'
    },
    {
        name: 'Counterparty Risk Management',
        y: 6.3,
        color: '#777777'
    },
    {
        name: 'Valuation Policy & Procedures',
        y: 6.3,
        color: '#969696'
    },
    {
        name: 'Pricing Sources',
        y: 6.3,
        color: '#DDDDDD'
    },
    {
        name: 'Illiquid Pricing',
        y: 6.3,
        color: '#EAEAEA'
    },
    {
        name: 'Accounting',
        y: 6.3,
        color: '#F8F8F8'
    }
];

export const oprDDRInnerChartData = [
    {
        name: "Governance ",
        y: 25,
        color: '#95B3D7'
    },
    {
        name: "Infrastructure",
        y: 25,
        color: '#366090'
    },
    {
        name: "Processes",
        y: 25,
        color: '#A6A6A6'
    },
    {
        name: "Valuation",
        y: 25,
        color: '#7F7F7F'
    }
];