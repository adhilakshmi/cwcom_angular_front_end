import { Component, OnInit, ViewChild} from '@angular/core';
import { Title } from '@angular/platform-browser';
import { environment } from '../../../environments/environment';
import { managerUpdatesData, hFProductDetailsData, pAProductDetailsData } from '../../data/dashboard/dashboard';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { CommonService } from '../../services/common.service';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  @ViewChild(DatatableComponent, {static: false}) private table: DatatableComponent;
  managerUpdatesDataRows = managerUpdatesData.results;
  hFProductDetailsData = hFProductDetailsData.results;
  pAProductDetailsData = pAProductDetailsData.results;
  currentPageLimit = 30;
  public pageLimitOptions = [
    {value: 15, text: '15'},
    {value: 30, text: '30'},
    {value: 50, text: '50'},
    {value: 100, text: '100'},
    {value: managerUpdatesData.results.length, text: 'All'},
  ];

  constructor(private titleService: Title,
    private commonService: CommonService
    ) { }

  ngOnInit() {
    this.titleService.setTitle(environment.pageTitle.dashboard +'-'+ environment.pageTitle.cliffwater);
  }

  public onLimitChange(limit) {
    this.commonService.onLimitChange(this.table,limit);
  }
  

}
