import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { environment } from '../../../environments/environment';
import { careerDetails, careerFootNote } from '../../data/careers/career-details';
import { CommonService } from '../../services/common.service';

@Component({
  selector: 'app-careers',
  templateUrl: './careers.component.html',
  styleUrls: ['./careers.component.scss']
})
export class CareersComponent implements OnInit {
  careerDetails : any[] = careerDetails;
  careerFootNote: any[] = careerFootNote;
  currentMonth : string;
  currentYear : number = 2020;

  constructor(private titleService: Title,
    private commonService: CommonService) { 
      this.currentMonth = this.commonService.getCurrentMonth();
      this.currentYear = this.commonService.getCurrentYear();
    }

  ngOnInit() {
    this.titleService.setTitle(environment.pageTitle.careers +'-'+ environment.pageTitle.cliffwater);
  }

}
