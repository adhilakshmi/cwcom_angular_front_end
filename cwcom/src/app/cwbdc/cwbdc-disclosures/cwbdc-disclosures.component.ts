import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-cwbdc-disclosures',
  templateUrl: './cwbdc-disclosures.component.html',
  styleUrls: ['./cwbdc-disclosures.component.scss']
})
export class CwbdcDisclosuresComponent implements OnInit {

  constructor(private titleService: Title) { }

  ngOnInit() {
    this.titleService.setTitle(environment.pageTitle.cwbdc+environment.pageTitle.disclosures +'-'+ environment.pageTitle.cliffwater);
  }

}
