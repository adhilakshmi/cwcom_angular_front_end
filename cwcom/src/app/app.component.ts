import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { environment } from '../environments/environment';
import { CommonService } from './services/common.service';
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { Location } from "@angular/common";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  isLoggedIn = false;
  isAccountPage = false;
  sub: any;

  constructor(private titleService: Title,
    private router: Router,
    location: Location, private route: ActivatedRoute, 
    private commonService : CommonService) { 
      this.sub = this.router.events.subscribe(event => {
        if (event instanceof NavigationEnd) {
         this.isAccountPage = (location.path() == "/Account/ForgotPassword" || location.path().includes("/Account/Login")) ? true : false;
         if(this.commonService.getCurrentUserValue()) {
            this.isLoggedIn = true;
          } else {
            this.isLoggedIn = false;
          }
        }
      });
      
    }

  ngOnInit() {
    this.titleService.setTitle(environment.pageTitle.cliffwater);
  }

  /*
  * Method to get User login details
  ********************************************************************************************/
  /*getUserDetail(userDet: string) {
    if(userDet) {
      this.isLoggedIn = true;
    } else {
      this.isLoggedIn = false;
     // this.router.navigate(['/Home']);
    }
  }*/


}
