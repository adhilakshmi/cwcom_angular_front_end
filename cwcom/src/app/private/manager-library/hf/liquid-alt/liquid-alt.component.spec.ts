import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LiquidAltComponent } from './liquid-alt.component';

describe('LiquidAltComponent', () => {
  let component: LiquidAltComponent;
  let fixture: ComponentFixture<LiquidAltComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LiquidAltComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LiquidAltComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
