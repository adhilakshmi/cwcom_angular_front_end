import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { dataName, dataAsset } from '../../data/multi-assets/multi-assets-pie-chart';
import { returnRiskData } from '../../data/multi-assets/multi-assets-scatter-chart';
import { Title } from '@angular/platform-browser';
import { environment } from '../../../environments/environment';
import { MatDialog } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-multi-asset',
  templateUrl: './multi-asset.component.html',
  styleUrls: ['./multi-asset.component.scss']
})
export class MultiAssetComponent implements OnInit {
  @ViewChild("emailReqPopup", {static: false}) emailReqPopup: TemplateRef<any>;
  pieChartOptions: any;
  scatterChartOptions : any;
  plotLineId = 'myPlotLine'; // To identify for removal
  emailReqForm: FormGroup;
  submitted : boolean = false;
  loading : boolean = false;
  environment = environment;
 

  // Plot line options for adding
  plotLineOptions = {
      color: '#FF0000',
      id: this.plotLineId,
      width: 2,
      value: 2.1,
      dashStyle: 'shortdash',
      label: {
          text: 'CPI:' + " 2.10%",
          align: 'right',
          x: -9,
          y: -12
      }
  };

  constructor(private titleService: Title, 
    public dialog: MatDialog,
    private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.titleService.setTitle(environment.pageTitle.multiAsset +'-'+ environment.pageTitle.cliffwater);
    this.generateSamplePortfolioChart();
    this.generateReturnRiskChart();
    /****************Email Required Form Validation****************** */
    this.emailReqForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
    });

    
  }

  /***********************************10-Year Return and Risk Forecasts - Scatter Chart***************************************/
  generateReturnRiskChart() {
    this.scatterChartOptions = {
      chart: {
        type: 'scatter',
        height: 570,
        width: screen.width < 768 ? "320" : "850",
        spacingTop: 10,
        marginRight: 30,
        plotBorderWidth: 0,
        spacingBottom: 0,
        animation: {
            duration: 1000
        },
        zoomType: 'xy',
      },
      legend: {
          enabled: true,
          align: 'left',
          verticalAlign: 'top',
          x: 0,
      },
      title: {
          text: '',
          align: 'center',
          fontWeight: 'bold'
      },
      subtitle: {
          text: ''
      },
      credits: {
          enabled: true
      },
      xAxis: {
          tickInterval: 5,
          gridLineWidth: 1,
          offset: -50,
          title: {
              text: 'Risk'
          },
          labels: {
              formatter: function () {
                  return this.value + " %";
              }
          },
          events: {
              afterSetExtremes: function (min, max) {
                  console.log('after set extremes');
                 // updateRegressionLine(min, max);
              }
          }
      },
      yAxis: {
          softMax: 10,
          endOnTick: false,
          startOnTick: false,
          plotLines: [
              this.plotLineOptions
          ],
          softMin: -2,
          title: {
              text: 'Return'
          },
          labels: {
              formatter: function () {
                  return this.value + " %";
              }
          },
          maxPadding: 0.2
      },
      tooltip: {
          useHTML: true,
          headerFormat: '<table>',
          pointFormat:
              '<div><b>{point.name}</b></div>' +
              '<div>Return:{point.y:.1f}%' +
              '<br>Risk: {point.x:.1f}%</div>',
          footerFormat: '</table>',
          followPointer: true
      },
      plotOptions: {
          series: {
              dataLabels: {
                  enabled: true,
                  allowOverlap: true,
                  groupPadding: 0.01,
                  pointPadding: 0.25,
                  padding: 0,
                  color: '#000000',
                  format: '{point.name}'
              },
              jitter: {
                  x: 0.25,
                  y: 0
              },
              events: {
                  legendItemClick: function (a, b, c) {
                      setTimeout(function () {
                         // updateRegressionLine();
                      }, 100);
                  }
              }
          }
      },
      series: [{
          data: returnRiskData[0].equityData,
          color: returnRiskData[0].eqColor,
          name: 'Equity',
          dataLabels: {
              padding: -3,
              y: -10,
              align: 'right'
          },
          marker: {
              symbol: 'circle'
          }
      }, {
          data: returnRiskData[0].creditData,
          color: returnRiskData[0].creditColor,
          name: 'Credit',
          dataLabels: {
              padding: -3,
              y: -10,
              align: 'left'
          },
          marker: {
              symbol: 'circle'
          }
      }, {
          data: returnRiskData[0].realAssetsData,
          color: returnRiskData[0].realAssetsColor,
          name: 'Real Assets',
          dataLabels: {
              padding: -3,
              y: 10,
              x: -6,
              align: 'right'
          },
          marker: {
              symbol: 'circle'
          }
      },
      {
          data: returnRiskData[0].ratesData,
          color: returnRiskData[0].ratesColor,
          name: 'Rates',
          dataLabels: {
              padding: -3,
              y: -10,
              align: 'left'
          },
          marker: {
              symbol: 'circle'
          }
      },
      {
        data: {},
        color: 'red',
        type: 'line',
        dashStyle: 'shortdash',
        lineWidth: 3,
        name: 'CPI',
        dataLabels: {
        },
        marker: {
            enabled: false
        },
        events: {
            legendItemClick: function (e) {
                if (this.visible) {
                    this.chart.yAxis[0].removePlotLine(this.plotLineId);
                }
                else {
                    this.chart.yAxis[0].addPlotLine(this.plotLineOptions);
                }
            }
        }
      },
      {
        type: 'line',
        name: 'Capital Market Line',
        data: [[2, 3.0843120773169277], [31, 14.291201411438399]],//added for temporary purpose
        marker: {
            enabled: false
        }
      }
    ],
    }
  }

  /***********************************Sample Portfolio - Pie Chart***************************************/
  generateSamplePortfolioChart() {
    this.pieChartOptions = {
      chart: {
        animation: {
          duration: 300,
        },
        type: 'pie',
        width: screen.width < 768 ? "320" : "500",
      },
      tooltip: {
        useHTML: true,
        backgroundColor: 'rgba(247,247,247,0.9)',
        headerFormat: '<table>',
        pointFormat: '<div style="font-size:14px">{point.name}</div>' +
        '<div style="font-size:13px">Allocation: {point.y:.0f}%',
        footerFormat: '</table>',
        followPointer: true
      },
      legend: {
        enabled: true,
        align: 'left',
        verticalAlign: 'top',
        x: 0
        /*y: -10,
        symbolHeight: 12,
        itemDistance: 12,
        padding: 0,
        itemHoverStyle: {
          color: '#000000',
          cursor: 'default'
        },
        symbolWidth: 12,
        symbolRadius: 0,
        title: {
          text: ''
        }*/
      },
      title: {
        text: '',
        align: 'center',
        style: {
          fontSize: '20px',
          fontWeight: 'bold',
          border: '1px solid #379695'
        }
      },
      subtitle: {
        align: 'center',
        text: "",
        style: {
          fontSize: '16px',
          fontWeight: 'bold',
          color: '#000000'
        }
      },
      credits: {
          enabled: true
      },
      xAxis : null,
      yAxis : null,
      plotOptions: {
        series: {
          animation: false,
          point: {
            events: {
              legendItemClick: function () {
                return false;
              }
            }
          }
        },
        pie: {
          dataLabels: {
            distance: 20,
            padding: 3,
            formatter: function () {
              if (this.y > 0) return this.key;
              else return null;
            },
            connectorPadding: 4,
            style: {
              width: '80px',
              fontSize: '12px',
              color: '#000000',
              fontWeight: 'normal'
            },
          },
        }
      },
      series: [{
        name: 'Category',
        size: '55%',
        innerSize: '45%',
        data: dataName
      },
      {
          name: 'Asset level',
          size: '30%',
          innerSize: '70%',
          showInLegend: false,
          data: dataAsset,        
      }],
    }
  }

  // convenience getter for easy access to form fields
  get f() { return this.emailReqForm.controls; }

  /**********************************Method to Submit Email Required Form***************************************/
  emailReqFormSubmit() {
    this.submitted = true;
      // stop here if form is invalid
      if (this.emailReqForm.invalid) {
          return;
      }
      this.loading = true;
      window.open(
        "https://cliffwater.com/Research/ReadFile?path=docs/Long Term (10 Yr.) Capital Market Assumptions 1Q 2019.pdf&title=Long Term (10 Yr.) Capital Market Assumptions 1Q 2019",
        '_blank' // <- This is what makes it open in a new window.
      );
      this.dialogClose();
      this.loading = false;
  }

  /**********************************Method to Open Email Required Popup***************************************/
  openEmailReqPopup(){
    /****************Email Required Form Validation****************** */
    this.emailReqForm.reset();
    this.dialog.open(this.emailReqPopup);
  }

  /**********************************Method to Close Opened Popup***************************************/
  dialogClose()
  {
    this.dialog.closeAll();
  }


}
