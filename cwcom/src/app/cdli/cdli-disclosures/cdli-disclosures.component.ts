import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-cdli-disclosures',
  templateUrl: './cdli-disclosures.component.html',
  styleUrls: ['./cdli-disclosures.component.scss']
})
export class CdliDisclosuresComponent implements OnInit {

  constructor(private titleService: Title) { }

  ngOnInit() {
    this.titleService.setTitle(environment.pageTitle.cdli+environment.pageTitle.disclosures +'-'+ environment.pageTitle.cliffwater);
  }

}
