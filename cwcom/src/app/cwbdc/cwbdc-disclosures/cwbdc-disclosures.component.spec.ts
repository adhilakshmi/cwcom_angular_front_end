import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CwbdcDisclosuresComponent } from './cwbdc-disclosures.component';

describe('CwbdcDisclosuresComponent', () => {
  let component: CwbdcDisclosuresComponent;
  let fixture: ComponentFixture<CwbdcDisclosuresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CwbdcDisclosuresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CwbdcDisclosuresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
