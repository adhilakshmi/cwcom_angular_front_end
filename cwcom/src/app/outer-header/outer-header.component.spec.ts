import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OuterHeaderComponent } from './outer-header.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material';
import { RouterTestingModule } from '@angular/router/testing';

describe('OuterHeaderComponent', () => {
  let component: OuterHeaderComponent;
  let fixture: ComponentFixture<OuterHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        MatDialogModule,
        RouterTestingModule
      ],
      declarations: [ OuterHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OuterHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
