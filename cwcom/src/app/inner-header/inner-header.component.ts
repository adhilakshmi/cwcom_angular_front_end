import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { LoginService } from '../services/login.service';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { searchFundsData } from '../data/dashboard/search-funds';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-inner-header',
  templateUrl: './inner-header.component.html',
  styleUrls: ['./inner-header.component.scss']
})
export class InnerHeaderComponent implements OnInit {
 // @Output() userDetail = new EventEmitter<string>();
  searchFundsControl: FormControl = new FormControl();
  searchOptions : any[]= searchFundsData;
  filteredOptions: Observable<string[]>;
  searchForm : FormGroup;

  constructor(
    private loginService : LoginService,
    private router: Router,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    //Input enter event in Search Field
    this.filteredOptions = this.searchFundsControl.valueChanges
      .pipe(
        startWith(''),
        map(val => val.length >= 1 ? this.filter(val): [])
      );
      /****************Search Form Init****************** */
      this.searchForm = this.formBuilder.group({
        searchFundsControl: ['']
      });
  }

  /**
   * Method to filter searched funds
   ***************************************************************************************************/
  filter(val: string): string[] {
      return this.searchOptions.filter(option =>
        option.fundName.toLowerCase().includes(val.toLowerCase()));
  }

  /**
   * Method to call Logout API
   ***************************************************************************************************/
  userLogout() {
    this.loginService.logout();
    //this.userDetail.emit('');
    this.router.navigate(['/Home']);
  }

}
