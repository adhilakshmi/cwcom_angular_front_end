export const searchFundsData  = [ 
    { 
       "fundId":1796471,
       "holdingId":0,
       "fundName":"1798 Volantis Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1150731,
       "holdingId":0,
       "fundName":"36 South Kohinoor Series Three Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1794795,
       "holdingId":0,
       "fundName":"37 Capital bluescale Fund, LP",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1257611,
       "holdingId":0,
       "fundName":"400 Capital Credit Opportunities Fund LP",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1622045,
       "holdingId":0,
       "fundName":"A.R.T. International Holdings (BVI) Ltd.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1873505,
       "holdingId":0,
       "fundName":"AB Arya Partners",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1543637,
       "holdingId":0,
       "fundName":"ADG Systematic Macro Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1150741,
       "holdingId":0,
       "fundName":"Advent Global Partners (Cayman)  Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1742085,
       "holdingId":0,
       "fundName":"Aeolus Property Catastrophe Keystone PF Fund LP",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1810803,
       "holdingId":0,
       "fundName":"Aeolus Property Catastrophe Spire PF Fund LP",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1177929,
       "holdingId":0,
       "fundName":"Aesir Credit Master Fund, Ltd.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135049,
       "holdingId":0,
       "fundName":"AG Diversified Income Fund Plus, L.P.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1282123,
       "holdingId":0,
       "fundName":"AG Diversified Income Fund, L.P.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135409,
       "holdingId":0,
       "fundName":"AG Mortgage Value Partners, LP",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135055,
       "holdingId":0,
       "fundName":"AG Super Fund, L.P.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1401341,
       "holdingId":0,
       "fundName":"AHL Alpha (Cayman) Limited",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1645647,
       "holdingId":0,
       "fundName":"AHL Dimension",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1401073,
       "holdingId":0,
       "fundName":"AHL Evolution",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1699617,
       "holdingId":0,
       "fundName":"AHL Evolution Frontier",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1150743,
       "holdingId":0,
       "fundName":"AKO Partners",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1459469,
       "holdingId":0,
       "fundName":"Aleutian Fund, Ltd.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1150737,
       "holdingId":0,
       "fundName":"Algert Global Equity Market Neutral Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1394715,
       "holdingId":18808887,
       "fundName":"AllianceBernstein U.S. Real Estate Partners II",
       "assetClass":"Real Estate"
    },
    { 
       "fundId":1805953,
       "holdingId":0,
       "fundName":"AlphaCat Advantage Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1805957,
       "holdingId":0,
       "fundName":"AlphaCat Diversified Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135061,
       "holdingId":0,
       "fundName":"Alphadyne International Fund Ltd.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1267177,
       "holdingId":0,
       "fundName":"AlphaQuest Original",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1265041,
       "holdingId":0,
       "fundName":"Altum Credit Fund, Ltd.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135413,
       "holdingId":0,
       "fundName":"Alyeska Fund, L.P.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135525,
       "holdingId":0,
       "fundName":"American Steadfast, LP",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1596359,
       "holdingId":0,
       "fundName":"Anchor Bolt Fund, LP",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135079,
       "holdingId":0,
       "fundName":"Anchorage Capital Partners, L.P.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1267319,
       "holdingId":0,
       "fundName":"Andurand Commodities Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1393813,
       "holdingId":0,
       "fundName":"Apollo Credit Strategies Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151513,
       "holdingId":0,
       "fundName":"AQR DELTA Fund II, LP",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1150763,
       "holdingId":0,
       "fundName":"AQR Global Stock Selection",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1781659,
       "holdingId":0,
       "fundName":"AQR Liquid Enhanced Alternative Premia Fund, L.P.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1150765,
       "holdingId":0,
       "fundName":"AQR Managed Futures",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1264783,
       "holdingId":0,
       "fundName":"AQR Style Premia Fund, L.P.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1388917,
       "holdingId":18914803,
       "fundName":"Arbor Investments IV, L.P.",
       "assetClass":"Private Equity"
    },
    { 
       "fundId":1135083,
       "holdingId":0,
       "fundName":"Archipelago Partners, L.P.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1150775,
       "holdingId":0,
       "fundName":"Aristeia Partners, L.P.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1659669,
       "holdingId":0,
       "fundName":"Arkkan Opportunities Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1741927,
       "holdingId":0,
       "fundName":"ARP Alternative Risk Premia Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1425349,
       "holdingId":0,
       "fundName":"ArrowMark Income Opportunity Fund, L.P.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135089,
       "holdingId":0,
       "fundName":"Ascend Partners Fund II BPO, Ltd.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1735609,
       "holdingId":0,
       "fundName":"ASG Managed Futures Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1681041,
       "holdingId":0,
       "fundName":"Ashoka Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1152095,
       "holdingId":0,
       "fundName":"Asian Century Quest Institutional Equity Fund, Ltd.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1918033,
       "holdingId":0,
       "fundName":"Aspect Core Trend Fund, Ltd.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1150789,
       "holdingId":0,
       "fundName":"Aspect Diversified Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1627715,
       "holdingId":0,
       "fundName":"Astignes Asia Rates Master Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1353709,
       "holdingId":18777397,
       "fundName":"Atalaya Asset Income Fund II",
       "assetClass":"Private Equity"
    },
    { 
       "fundId":1561247,
       "holdingId":18908595,
       "fundName":"Atalaya Asset Income Fund III",
       "assetClass":"Private Equity"
    },
    { 
       "fundId":1779681,
       "holdingId":19024971,
       "fundName":"Atalaya Asset Income Fund IV LP",
       "assetClass":"Private Equity"
    },
    { 
       "fundId":1465643,
       "holdingId":18870267,
       "fundName":"Atalaya Special Opportunities Fund VI, L.P.",
       "assetClass":"Private Equity"
    },
    { 
       "fundId":1756331,
       "holdingId":19071911,
       "fundName":"Atalaya Special Opportunities Fund VII, L.P.",
       "assetClass":"Private Equity"
    },
    { 
       "fundId":1433433,
       "holdingId":18849043,
       "fundName":"Athyrium Opportunities Fund II",
       "assetClass":"Private Equity"
    },
    { 
       "fundId":1632937,
       "holdingId":18932625,
       "fundName":"Athyrium Opportunities Fund III",
       "assetClass":"Private Equity"
    },
    { 
       "fundId":1150793,
       "holdingId":0,
       "fundName":"Atlas Global Investments",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135419,
       "holdingId":0,
       "fundName":"Aurelius Capital",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135421,
       "holdingId":0,
       "fundName":"Autonomy Global Macro Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1183559,
       "holdingId":0,
       "fundName":"Axonic Credit Opportunities Fund, LP",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1933193,
       "holdingId":0,
       "fundName":"Bardin-Hill Event Driven Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1681175,
       "holdingId":0,
       "fundName":"Barnegat Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151515,
       "holdingId":0,
       "fundName":"Baupost Value Partners",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135109,
       "holdingId":0,
       "fundName":"Bay Pond Partners, L.P.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1150805,
       "holdingId":0,
       "fundName":"Bay Resource Partners",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1676427,
       "holdingId":0,
       "fundName":"Beach Point Securitized Credit Fund LP",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151895,
       "holdingId":0,
       "fundName":"Beach Point Total Return Fund II LP",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1404267,
       "holdingId":0,
       "fundName":"BFAM Asian Opportunities Fund, LP",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1150809,
       "holdingId":0,
       "fundName":"BG Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1150845,
       "holdingId":0,
       "fundName":"BH-DG Systematic Trading Master Fund Limited",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151893,
       "holdingId":0,
       "fundName":"BIL, Ltd.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1402701,
       "holdingId":0,
       "fundName":"Birch Grove Credit Strategies Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1150811,
       "holdingId":0,
       "fundName":"Black Diamond",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1842377,
       "holdingId":0,
       "fundName":"Black Diamond Arbitrage",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1152097,
       "holdingId":0,
       "fundName":"Black River Global Multi-Strategy Leveraged Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1248399,
       "holdingId":0,
       "fundName":"BlackRock 32 Capital Fund Ltd.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1152065,
       "holdingId":0,
       "fundName":"BlackRock Credit (Offshore) Investors II, LP",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1475435,
       "holdingId":0,
       "fundName":"BlackRock Credit Alpha",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1542421,
       "holdingId":0,
       "fundName":"Blackrock European Hedge Fund Limited",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1150813,
       "holdingId":0,
       "fundName":"BlackRock Fixed Income Global Alpha Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1150817,
       "holdingId":0,
       "fundName":"BlackRock Obsidian Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1475433,
       "holdingId":0,
       "fundName":"BlackRock Pan Asia Opportunities Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1768963,
       "holdingId":0,
       "fundName":"BlackRock Style Advantage Strategy",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1629541,
       "holdingId":0,
       "fundName":"Bloom Tree Fund, LP",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1666299,
       "holdingId":18985683,
       "fundName":"Blue Point Capital Partners IV (A), L.P",
       "assetClass":"Private Equity"
    },
    { 
       "fundId":1439085,
       "holdingId":0,
       "fundName":"BlueBay Credit Alpha Long Short Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1150831,
       "holdingId":0,
       "fundName":"BlueMatrix Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1150823,
       "holdingId":0,
       "fundName":"BlueMountain Global Volatility Fund L.P.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1352855,
       "holdingId":0,
       "fundName":"BlueSpruce Fund LP",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1948837,
       "holdingId":0,
       "fundName":"Bodenholm",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1729259,
       "holdingId":0,
       "fundName":"Brevan Howard AH Fund, L.P.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135429,
       "holdingId":0,
       "fundName":"Brevan Howard Asia Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135377,
       "holdingId":0,
       "fundName":"Brevan Howard Fund Limited",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135381,
       "holdingId":0,
       "fundName":"Brevan Howard Multi-Strategy Fund Limited",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1483459,
       "holdingId":0,
       "fundName":"Bridgewater Optimal Portfolio",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1160439,
       "holdingId":0,
       "fundName":"Bridgewater Pure Alpha Fund II, Ltd.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1182547,
       "holdingId":0,
       "fundName":"Bridgewater Pure Alpha Major Markets, LLC",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135125,
       "holdingId":0,
       "fundName":"Bridgewater Pure Alpha Trading Company Ltd.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135127,
       "holdingId":0,
       "fundName":"Brigade Leveraged Capital Structures Fund LP",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1719137,
       "holdingId":0,
       "fundName":"Broad Reach Master Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1404939,
       "holdingId":0,
       "fundName":"Brookdale International Partners, L.P.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1150851,
       "holdingId":0,
       "fundName":"Brummer Multi-Strategy",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1150853,
       "holdingId":0,
       "fundName":"BTG Pactual GEMM",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1638899,
       "holdingId":0,
       "fundName":"Bybrook Capital Master Fund LP",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135131,
       "holdingId":0,
       "fundName":"Cadian Fund LP",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1305731,
       "holdingId":0,
       "fundName":"Cambrian Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1232119,
       "holdingId":0,
       "fundName":"Campbell Global Assets Fund Limited – Class A Shares",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1152111,
       "holdingId":0,
       "fundName":"Canyon Balanced Fund, LP",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135135,
       "holdingId":0,
       "fundName":"Canyon Value Realization Fund (Cayman), Ltd.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135137,
       "holdingId":0,
       "fundName":"Canyon Value Realization Fund, L.P.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1150863,
       "holdingId":0,
       "fundName":"Capstone Volatility",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135141,
       "holdingId":0,
       "fundName":"Capula Global Relative Value Fund Ltd.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135143,
       "holdingId":0,
       "fundName":"Capula Tail Risk Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1907615,
       "holdingId":0,
       "fundName":"Capula Volatility Opportunities Trust",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1321147,
       "holdingId":0,
       "fundName":"Carrhae Capital Fund, Ltd.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135145,
       "holdingId":0,
       "fundName":"Caspian Select Credit Fund, L.P.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1150867,
       "holdingId":0,
       "fundName":"Castle Creek ARB Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135151,
       "holdingId":0,
       "fundName":"Caxton Global Investments Limited",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1384821,
       "holdingId":0,
       "fundName":"CC&L Market Neutral Strategy",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1249405,
       "holdingId":0,
       "fundName":"CCP Core Macro Fund LP",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151485,
       "holdingId":0,
       "fundName":"CCP Quantitative Fund LP",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135539,
       "holdingId":0,
       "fundName":"Centerbridge Credit Partners, L.P.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135153,
       "holdingId":0,
       "fundName":"Cevian Capital II, LP",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1393681,
       "holdingId":0,
       "fundName":"CFM Institutional Systematic Diversified Fund LP",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1393725,
       "holdingId":0,
       "fundName":"CFM Stratus",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135435,
       "holdingId":0,
       "fundName":"Chatham Asset Partners High Yield Fund, LP",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1150879,
       "holdingId":0,
       "fundName":"Chenavari Multi Strategy Credit Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1901013,
       "holdingId":0,
       "fundName":"CIAM Opportunities Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1660519,
       "holdingId":0,
       "fundName":"Citadel Fixed Income Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1627259,
       "holdingId":0,
       "fundName":"Citadel Global Equities",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1150885,
       "holdingId":0,
       "fundName":"Citadel Kensington Global Strategies",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135157,
       "holdingId":0,
       "fundName":"Claren Road Credit Fund, Ltd.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135439,
       "holdingId":0,
       "fundName":"Coatue Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1664697,
       "holdingId":0,
       "fundName":"Column Park Partners LP",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1394903,
       "holdingId":0,
       "fundName":"Complus Asia Macro Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1150893,
       "holdingId":0,
       "fundName":"Contrarian Capital Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1150895,
       "holdingId":0,
       "fundName":"Contrarian Emerging Markets",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1150897,
       "holdingId":0,
       "fundName":"Corsair Capital",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1150899,
       "holdingId":0,
       "fundName":"Corvex Partners, LP",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1150903,
       "holdingId":0,
       "fundName":"CQS ABS Feeder Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151649,
       "holdingId":0,
       "fundName":"CQS Directional Opportunities Feeder Fund, LP",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1242399,
       "holdingId":0,
       "fundName":"CQS Diversified Fund (SPC) Limited (Segregated Portfolio Alpha II)",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1150905,
       "holdingId":0,
       "fundName":"CQS Global Convertible Arbitrage Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1735595,
       "holdingId":0,
       "fundName":"Crabel Advanced Trend",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1150907,
       "holdingId":0,
       "fundName":"Crabel Multi-Product",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1173543,
       "holdingId":0,
       "fundName":"Credit Suisse Securitized Products Master Fund, Ltd.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1905283,
       "holdingId":0,
       "fundName":"Crescent Park Partners LP",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1424355,
       "holdingId":0,
       "fundName":"CVC Global Credit Opportunities Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1986997,
       "holdingId":0,
       "fundName":"D1 Capital Partners Onshore LP",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1249441,
       "holdingId":0,
       "fundName":"Davidson Kempner Distressed Opportunities Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135169,
       "holdingId":0,
       "fundName":"Davidson Kempner International, Ltd.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1467033,
       "holdingId":18808377,
       "fundName":"Davidson Kempner Long-Term Distressed Opportunities Fund III",
       "assetClass":"Private Equity"
    },
    { 
       "fundId":1766655,
       "holdingId":18984021,
       "fundName":"Davidson Kempner Long-Term Distressed Opportunities Fund IV LP",
       "assetClass":"Private Equity"
    },
    { 
       "fundId":1391117,
       "holdingId":18792177,
       "fundName":"Davidson Kempner Long-Term Distressed Opportunities Fund LP",
       "assetClass":"Private Equity"
    },
    { 
       "fundId":1827181,
       "holdingId":0,
       "fundName":"DCI Market Neutral Credit AIF Strategy",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135171,
       "holdingId":0,
       "fundName":"DE Shaw Composite International Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135173,
       "holdingId":0,
       "fundName":"DE Shaw Multi-Asset Fund, L.L.C.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135449,
       "holdingId":0,
       "fundName":"DE Shaw Oculus Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1405695,
       "holdingId":0,
       "fundName":"DE Shaw Orienteer Fund, L.L.C.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1629945,
       "holdingId":0,
       "fundName":"DE Shaw Valence Fund, L.L.C.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151651,
       "holdingId":0,
       "fundName":"Deerfield International",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1268721,
       "holdingId":0,
       "fundName":"Discovery Global Macro Partnership, L.P.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135175,
       "holdingId":0,
       "fundName":"Discovery Global Opportunity Fund, Ltd.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1473001,
       "holdingId":0,
       "fundName":"Dorsal Capital Partners, L.P.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151653,
       "holdingId":0,
       "fundName":"Double Black Diamond",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1400459,
       "holdingId":0,
       "fundName":"Double Haven Asia Absolute Bond Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1318801,
       "holdingId":0,
       "fundName":"Dragon Billion China Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1276251,
       "holdingId":0,
       "fundName":"DSAM Long / Short Equity Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135119,
       "holdingId":0,
       "fundName":"DW Catalyst Onshore Fund, LP",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1150923,
       "holdingId":0,
       "fundName":"Dymon Asia Macro Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1460509,
       "holdingId":0,
       "fundName":"East Bridge Capital Master Fund, Ltd",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1444555,
       "holdingId":0,
       "fundName":"East Lodge Capital Credit Opportunities Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1150925,
       "holdingId":0,
       "fundName":"ECF Value Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1307065,
       "holdingId":0,
       "fundName":"Echo Street Capital Partners Offshore Ltd.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1277047,
       "holdingId":0,
       "fundName":"ECM Master Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1183171,
       "holdingId":0,
       "fundName":"Egerton Long/Short (USD) Fund Limited",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1987259,
       "holdingId":0,
       "fundName":"Eisler Capital Master Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1303745,
       "holdingId":0,
       "fundName":"EJF Debt Opportunities Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":2003307,
       "holdingId":0,
       "fundName":"Elan Master Fund Ltd.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1703101,
       "holdingId":0,
       "fundName":"Electron Global Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1150929,
       "holdingId":0,
       "fundName":"Element Capital Feeder Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1152143,
       "holdingId":0,
       "fundName":"Elementum NatCat Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1150931,
       "holdingId":0,
       "fundName":"Ellington Credit Opportunities Partners",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1150933,
       "holdingId":0,
       "fundName":"Ellington Mortgage Opportunities Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135181,
       "holdingId":0,
       "fundName":"Elliott International Limited",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151523,
       "holdingId":0,
       "fundName":"Eminence Partners",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135453,
       "holdingId":0,
       "fundName":"Empyrean Capital Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135451,
       "holdingId":0,
       "fundName":"EMSO",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1559257,
       "holdingId":0,
       "fundName":"EMSO Saguaro Ltd.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1316261,
       "holdingId":18723529,
       "fundName":"EnCap Energy Capital VIII Co-Investors, L.P.",
       "assetClass":"Real Assets"
    },
    { 
       "fundId":1316263,
       "holdingId":18723531,
       "fundName":"EnCap Energy Capital VIII, L.P.",
       "assetClass":"Real Assets"
    },
    { 
       "fundId":1150941,
       "holdingId":0,
       "fundName":"Encompass Capital Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1316275,
       "holdingId":18723533,
       "fundName":"EnerVest Energy Institutional Fund XII",
       "assetClass":"Real Assets"
    },
    { 
       "fundId":1402363,
       "holdingId":0,
       "fundName":"Engaged Capital Flagship Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1923941,
       "holdingId":0,
       "fundName":"Engle Capital Master Fund Ltd",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1150943,
       "holdingId":0,
       "fundName":"Eos Credit Opportunities",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1150945,
       "holdingId":0,
       "fundName":"Farallon Capital Institutional Investors, LP",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1478483,
       "holdingId":0,
       "fundName":"Farmstead Capital Master Fund, Ltd.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1150949,
       "holdingId":0,
       "fundName":"Field Street Master Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1182353,
       "holdingId":0,
       "fundName":"Finisterre Credit Partners, LP",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1388919,
       "holdingId":18886283,
       "fundName":"Flexpoint Fund III, L.P.",
       "assetClass":"Private Equity"
    },
    { 
       "fundId":1557075,
       "holdingId":18886281,
       "fundName":"Flexpoint Special Assets Fund, L.P.",
       "assetClass":"Private Equity"
    },
    { 
       "fundId":1481427,
       "holdingId":0,
       "fundName":"FORT Global Contrarian Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1780287,
       "holdingId":0,
       "fundName":"Fort Global Diversified",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1943615,
       "holdingId":0,
       "fundName":"Franklin Systematic Global Premia Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1150965,
       "holdingId":0,
       "fundName":"Freshford Partners",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1687263,
       "holdingId":0,
       "fundName":"Fulcrum Multi-Asset Carry Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1687261,
       "holdingId":0,
       "fundName":"Fulcrum Multi-Asset Trend Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1687259,
       "holdingId":0,
       "fundName":"Fulcrum Multi-Asset Volatility Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1445351,
       "holdingId":0,
       "fundName":"Fundamental Credit Opportunities LP",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1171839,
       "holdingId":0,
       "fundName":"Gabelli Associates",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1387291,
       "holdingId":0,
       "fundName":"GAM Alternative Risk Premia Strategy",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1781581,
       "holdingId":0,
       "fundName":"GAM FCM Cat Bond - USD Open",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1150977,
       "holdingId":0,
       "fundName":"GAM Global Rates Hedge Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1257041,
       "holdingId":0,
       "fundName":"Gaoling",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1741989,
       "holdingId":0,
       "fundName":"Garda Fixed Income Relative Value Opportunity Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1216679,
       "holdingId":0,
       "fundName":"Gavea Master Fund Ltd.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1150979,
       "holdingId":0,
       "fundName":"GCA Credit Opportunities Fund, LLC",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1653195,
       "holdingId":0,
       "fundName":"GCM Offshore Equity Partners LP",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1763115,
       "holdingId":19077621,
       "fundName":"Genstar Capital Partners IX, L.P.",
       "assetClass":"Private Equity"
    },
    { 
       "fundId":1708315,
       "holdingId":18942883,
       "fundName":"Genstar Capital Partners VIII",
       "assetClass":"Private Equity"
    },
    { 
       "fundId":1150985,
       "holdingId":0,
       "fundName":"Gladwyne European Credit Opportunities Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1150987,
       "holdingId":0,
       "fundName":"Glazer Capital Management",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1666821,
       "holdingId":0,
       "fundName":"Glen Point Global Macro Fund Limited",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135189,
       "holdingId":0,
       "fundName":"Glenview Capital Partners (Cayman) Offshore, Ltd.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135191,
       "holdingId":0,
       "fundName":"Glenview Institutional Partners, L.P.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1569011,
       "holdingId":0,
       "fundName":"GLG Market Neutral Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1677585,
       "holdingId":0,
       "fundName":"GMO Mean Reversion",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1150997,
       "holdingId":0,
       "fundName":"GMO Systematic Global Macro Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1315005,
       "holdingId":0,
       "fundName":"Golden China Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135193,
       "holdingId":0,
       "fundName":"GoldenTree Credit Opportunities, L.P.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135197,
       "holdingId":0,
       "fundName":"GoldenTree Offshore Fund, Ltd.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151003,
       "holdingId":0,
       "fundName":"Good Hill Partners Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1594395,
       "holdingId":0,
       "fundName":"Governors Lane Onshore Fund LP",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135201,
       "holdingId":0,
       "fundName":"Gracie Credit Opportunities Fund, L.P.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1550433,
       "holdingId":0,
       "fundName":"Graham Absolute Return Trading Ltd.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1708493,
       "holdingId":0,
       "fundName":"Graham Global Investment Fund I SPC Ltd. - Absolute Return TT Capped Beta SP",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1180757,
       "holdingId":0,
       "fundName":"Graham Global Investment Fund I SPC Ltd. - Discretionary Enhanced Vol Segregated Portfolio",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135207,
       "holdingId":0,
       "fundName":"Graham Global Investment Fund I SPC Ltd. - Discretionary Segregated Portfolio",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135213,
       "holdingId":0,
       "fundName":"Graham Global Investment Fund I SPC Ltd. - K4D-10V Segregated Portfolio",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135215,
       "holdingId":0,
       "fundName":"Graham Global Investment Fund I SPC Ltd. - Proprietary Matrix Segregated Portfolio",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1941989,
       "holdingId":0,
       "fundName":"Graham Global Investment Fund I SPC Ltd. - Quant Macro Segregated Portfolio",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1685357,
       "holdingId":0,
       "fundName":"Graham Global Investment Fund I SPC Ltd. - Tactical Trend Capped Beta (Equities) Segregated Portfolio",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1769503,
       "holdingId":0,
       "fundName":"Graham Global Investment Fund II SPC Ltd. - Tactical Trend Segregated Portfolio",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1153131,
       "holdingId":0,
       "fundName":"Graticule Asia Macro Fund Ltd",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1276003,
       "holdingId":0,
       "fundName":"GreenCourt Greater China Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151005,
       "holdingId":0,
       "fundName":"Greylock Global Opportunity Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1387491,
       "holdingId":0,
       "fundName":"GS Alternative Risk Premia Strategy",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151013,
       "holdingId":0,
       "fundName":"GSA Capital International Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1660699,
       "holdingId":0,
       "fundName":"GSA Capital Trend Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1942461,
       "holdingId":0,
       "fundName":"GSA Diversified Alternatives Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1401181,
       "holdingId":0,
       "fundName":"GSAM Global Opportunities Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1314815,
       "holdingId":0,
       "fundName":"H2O Force 10",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1411051,
       "holdingId":18788053,
       "fundName":"Halstatt Real Estate Partners II",
       "assetClass":"Real Estate"
    },
    { 
       "fundId":1881801,
       "holdingId":19028741,
       "fundName":"Halstatt Real Estate Partners III, L.P.",
       "assetClass":"Real Estate"
    },
    { 
       "fundId":1906205,
       "holdingId":0,
       "fundName":"Hawk Ridge Partners LP",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151025,
       "holdingId":0,
       "fundName":"Hawkes Bay Partners",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1386269,
       "holdingId":0,
       "fundName":"HBK Multi-Strategy Offshore Fund Ltd.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135465,
       "holdingId":0,
       "fundName":"HealthCor",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1990291,
       "holdingId":0,
       "fundName":"Heard Opportunity Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1242519,
       "holdingId":0,
       "fundName":"HG Vora Special Opportunities Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1591893,
       "holdingId":0,
       "fundName":"Highbridge Tactial Credit & Convertibles Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1543765,
       "holdingId":0,
       "fundName":"Hitchwood Capital Fund Ltd",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1814425,
       "holdingId":0,
       "fundName":"Holocene Advisors Master Fund Ltd.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1703165,
       "holdingId":0,
       "fundName":"Honeycomb Partners",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1291299,
       "holdingId":0,
       "fundName":"Horizon Portfolio I Limited",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1742467,
       "holdingId":0,
       "fundName":"HPS Asia L/S Credit Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135469,
       "holdingId":0,
       "fundName":"Hudson Bay Fund LP",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1680547,
       "holdingId":0,
       "fundName":"Hunt Lane Capital",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1318133,
       "holdingId":0,
       "fundName":"Ichigo Trust",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1173389,
       "holdingId":0,
       "fundName":"Iguazu Partners, L.P.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1346731,
       "holdingId":18935761,
       "fundName":"Incline Equity Partners IV, L.P.",
       "assetClass":"Private Equity"
    },
    { 
       "fundId":1906935,
       "holdingId":19162809,
       "fundName":"Incline Equity Partners V",
       "assetClass":"Private Equity"
    },
    { 
       "fundId":1151045,
       "holdingId":0,
       "fundName":"Indaba Capital Partners",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135475,
       "holdingId":0,
       "fundName":"Indus Japan Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1963273,
       "holdingId":0,
       "fundName":"InfinityQ Volatility Alpha Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1724809,
       "holdingId":19036793,
       "fundName":"Inflexion Buyout Fund V",
       "assetClass":"Private Equity"
    },
    { 
       "fundId":1766657,
       "holdingId":19036795,
       "fundName":"Inflexion Partnership Capital Fund II, L.P.",
       "assetClass":"Private Equity"
    },
    { 
       "fundId":1267155,
       "holdingId":0,
       "fundName":"Informed Portfolio Management",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1847213,
       "holdingId":0,
       "fundName":"Intrinsic Edge Plus",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1666761,
       "holdingId":0,
       "fundName":"Ion Israel Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1632789,
       "holdingId":0,
       "fundName":"Ionic Volatility Arbitrage II Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1779361,
       "holdingId":0,
       "fundName":"IP All Seasons Asian Credit Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151053,
       "holdingId":0,
       "fundName":"ISAM Systematic",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151057,
       "holdingId":0,
       "fundName":"Janchor Partners",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151061,
       "holdingId":0,
       "fundName":"JD Capital Tempo Volatility Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1845093,
       "holdingId":0,
       "fundName":"Jennison Global Healthcare Master Fund, Ltd.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151063,
       "holdingId":0,
       "fundName":"Jet Capital Concentrated Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151065,
       "holdingId":0,
       "fundName":"JHL Capital",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151067,
       "holdingId":0,
       "fundName":"JLP Credit Opportunities Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1387573,
       "holdingId":0,
       "fundName":"JPMCB Systematic Alpha Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1251309,
       "holdingId":0,
       "fundName":"JPS Credit Opportunities Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1803917,
       "holdingId":0,
       "fundName":"Juniper Catastrophe Fund L.P.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1948133,
       "holdingId":0,
       "fundName":"Junto Capital Partners LP",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151069,
       "holdingId":0,
       "fundName":"Kaiser Trading Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1261079,
       "holdingId":0,
       "fundName":"Karya Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151073,
       "holdingId":0,
       "fundName":"Kepos Capital Alpha Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1399529,
       "holdingId":0,
       "fundName":"Kepos Exotic Beta Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1267089,
       "holdingId":0,
       "fundName":"Keynes Leveraged Quantitative Strategies Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1257447,
       "holdingId":0,
       "fundName":"Kildonan Castle Global Credit Opportunity Fund, LLC",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135231,
       "holdingId":0,
       "fundName":"King Street Capital, L.P.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135233,
       "holdingId":0,
       "fundName":"King Street Capital, Ltd.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135481,
       "holdingId":0,
       "fundName":"King Street Europe LP",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151077,
       "holdingId":0,
       "fundName":"Kingdon Credit Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135239,
       "holdingId":0,
       "fundName":"KLS Diversified Fund Ltd.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151081,
       "holdingId":0,
       "fundName":"Knighthead Master Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1810791,
       "holdingId":0,
       "fundName":"Kuvari Focus Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135483,
       "holdingId":0,
       "fundName":"Kylin Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1161275,
       "holdingId":0,
       "fundName":"Lakewood Capital Partners, LP",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135487,
       "holdingId":0,
       "fundName":"Lansdowne Developed Markets Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151087,
       "holdingId":0,
       "fundName":"Latigo Ultra Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135489,
       "holdingId":0,
       "fundName":"Laurion Capital, LP",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135243,
       "holdingId":0,
       "fundName":"Lazard Rathmore Fund, Ltd.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1801115,
       "holdingId":0,
       "fundName":"Leadenhall Diversified Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1812997,
       "holdingId":0,
       "fundName":"Leadenhall Value Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1943947,
       "holdingId":0,
       "fundName":"LFIS Vision - Premia Opportunities",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1433341,
       "holdingId":0,
       "fundName":"Light Street Halogen, L.P.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135245,
       "holdingId":0,
       "fundName":"LIM Asia Multi-Strategy Fund, Inc",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1316705,
       "holdingId":18723535,
       "fundName":"Lime Rock Partners VI, L.P.",
       "assetClass":"Real Assets"
    },
    { 
       "fundId":1135491,
       "holdingId":0,
       "fundName":"Linden Investors",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1242285,
       "holdingId":0,
       "fundName":"LMR Fund Limited",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1664481,
       "holdingId":0,
       "fundName":"Lodbrok Long/Short European Credit Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1942717,
       "holdingId":0,
       "fundName":"LOIM Alternative Risk Premia",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1279847,
       "holdingId":0,
       "fundName":"Lomas Capital Master Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151093,
       "holdingId":0,
       "fundName":"Lone Pine - Lone Cypress",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1546175,
       "holdingId":0,
       "fundName":"Long Pond Capital QP Fund, LP",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151525,
       "holdingId":0,
       "fundName":"Lubben Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1717799,
       "holdingId":0,
       "fundName":"Luminus Energy Partners",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135247,
       "holdingId":0,
       "fundName":"Lynx (Bermuda) Ltd.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151099,
       "holdingId":0,
       "fundName":"M&G Episode",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1171305,
       "holdingId":0,
       "fundName":"MacroSynergy",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1316791,
       "holdingId":18901823,
       "fundName":"Madison Dearborn Partners VII",
       "assetClass":"Private Equity"
    },
    { 
       "fundId":1674959,
       "holdingId":0,
       "fundName":"Magdalena Partners, L.P.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1942773,
       "holdingId":0,
       "fundName":"Magnetar Multi-Strategy ARP Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1741801,
       "holdingId":0,
       "fundName":"Man Alternative Risk Premia - Class A",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151109,
       "holdingId":0,
       "fundName":"Manikay Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135251,
       "holdingId":0,
       "fundName":"Marathon Special Opportunity Fund, L.P.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135253,
       "holdingId":0,
       "fundName":"Marathon Special Opportunity Fund, Ltd.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1549453,
       "holdingId":0,
       "fundName":"Marble Ridge",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1470451,
       "holdingId":0,
       "fundName":"Marblegate Special Opportunities Master Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151113,
       "holdingId":0,
       "fundName":"Mariner Partners",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151117,
       "holdingId":0,
       "fundName":"Marshall Wace Funds LP - MW Eureka (US) Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1243605,
       "holdingId":0,
       "fundName":"Marshall Wace Funds LP - MW Global Opportunities (US) Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151119,
       "holdingId":0,
       "fundName":"Marshall Wace Funds LP - MW TOPS (US) Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1709979,
       "holdingId":0,
       "fundName":"Marshall Wace Funds plc - MW Japan Market Neutral Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1404265,
       "holdingId":0,
       "fundName":"Maso Capital Master Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151123,
       "holdingId":0,
       "fundName":"Maverick Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1790273,
       "holdingId":0,
       "fundName":"Melqart Opportunities Master Fund Ltd",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1476895,
       "holdingId":0,
       "fundName":"Menta Global Offshore, Ltd",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1482169,
       "holdingId":0,
       "fundName":"MidOcean Absolute Return Credit Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151127,
       "holdingId":0,
       "fundName":"MidOcean Credit Opportunity Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1908783,
       "holdingId":0,
       "fundName":"MIG Master Fund, LP",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151129,
       "holdingId":0,
       "fundName":"Millburn Multi-Markets",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151131,
       "holdingId":0,
       "fundName":"Millennium",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151133,
       "holdingId":0,
       "fundName":"Miura Global",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135495,
       "holdingId":0,
       "fundName":"MKP Opportunity Partners, LP",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151135,
       "holdingId":0,
       "fundName":"MLM Macro - Peak",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1710827,
       "holdingId":0,
       "fundName":"Monarch Debt Recovery Fund, Ltd.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151139,
       "holdingId":0,
       "fundName":"Moon Capital Global Equity Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135265,
       "holdingId":0,
       "fundName":"Moore Global Investments, Ltd.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135269,
       "holdingId":0,
       "fundName":"Moore Macro Managers Fund, Ltd.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151143,
       "holdingId":0,
       "fundName":"MSD Credit Opportunities Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151145,
       "holdingId":0,
       "fundName":"MSD European Opportunity Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1445969,
       "holdingId":0,
       "fundName":"Mudrick Distressed Opportunity Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151665,
       "holdingId":0,
       "fundName":"Myriad Opportunities US Fund Limited",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1481251,
       "holdingId":0,
       "fundName":"Napier Park Select Fund, LP",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151147,
       "holdingId":0,
       "fundName":"Naya Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1950685,
       "holdingId":0,
       "fundName":"NB Insurance-Linked Strategies Fund LP",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1687857,
       "holdingId":0,
       "fundName":"Nephila Catastrophe Fund L.P.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151667,
       "holdingId":0,
       "fundName":"Newbrook Capital",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1316903,
       "holdingId":18723537,
       "fundName":"NGP Natural Resources X, LP",
       "assetClass":"Real Assets"
    },
    { 
       "fundId":1405563,
       "holdingId":0,
       "fundName":"Nine Masts Feeder Fund I",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1913871,
       "holdingId":0,
       "fundName":"Nineteen77 Global Merger Arbitrage",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1438923,
       "holdingId":0,
       "fundName":"Nipun Asia Total Return Fund L.P.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1414115,
       "holdingId":0,
       "fundName":"North MaxQ Macro Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1895025,
       "holdingId":0,
       "fundName":"Northlight European Fundamental Credit Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151153,
       "holdingId":0,
       "fundName":"Northwest Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1402657,
       "holdingId":0,
       "fundName":"Numeric Absolute Return Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1402655,
       "holdingId":0,
       "fundName":"Numeric Alternative Market Neutral Strategy",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1267081,
       "holdingId":0,
       "fundName":"NWI",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1443897,
       "holdingId":0,
       "fundName":"OCCO Eastern European Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151167,
       "holdingId":0,
       "fundName":"Oceanwood Capital Management",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151157,
       "holdingId":0,
       "fundName":"OConnor Global Multi-Strategy Alpha",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1926325,
       "holdingId":0,
       "fundName":"Omni Event Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151173,
       "holdingId":0,
       "fundName":"One William Street Capital Partners",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1662095,
       "holdingId":0,
       "fundName":"Onex Debt Opportunity International, Ltd.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1249153,
       "holdingId":0,
       "fundName":"Orchard Square Partners Credit Fund Ltd",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135283,
       "holdingId":0,
       "fundName":"Owl Creek Overseas Fund, Ltd.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1954741,
       "holdingId":0,
       "fundName":"OWS Credit Opportunity Fund, L.P.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1953917,
       "holdingId":0,
       "fundName":"OWS Credit Opportunity Offshore Fund, Ltd.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151669,
       "holdingId":0,
       "fundName":"OxAM Quant Fund (US) LLC",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151185,
       "holdingId":0,
       "fundName":"P2 Capital Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151187,
       "holdingId":0,
       "fundName":"Pacific Alliance Asia Opportunity Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1784867,
       "holdingId":0,
       "fundName":"Pagosa Partners, L.P.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1300089,
       "holdingId":0,
       "fundName":"Palestra Capital Fund, LP",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1465895,
       "holdingId":0,
       "fundName":"Palmerston European Credit Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1803915,
       "holdingId":0,
       "fundName":"Palmetto Catastrophe Fund L.P.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151189,
       "holdingId":0,
       "fundName":"Paloma Partners",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1182969,
       "holdingId":0,
       "fundName":"Parallax Offshore Investors Fund Ltd.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1247663,
       "holdingId":0,
       "fundName":"Parsec Trading Corp.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135391,
       "holdingId":0,
       "fundName":"Paulson Advantage Ltd.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151203,
       "holdingId":0,
       "fundName":"Pelham Long/Short Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151205,
       "holdingId":0,
       "fundName":"Pentwater Event Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1682477,
       "holdingId":0,
       "fundName":"Pharo Gaia Fund, Ltd.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151209,
       "holdingId":0,
       "fundName":"Pharo Macro Fund, Ltd.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1794051,
       "holdingId":0,
       "fundName":"Pillar Capital Juniperus Insurance Opportunity Fund Limited",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151211,
       "holdingId":0,
       "fundName":"PIMCO Absolute Return Strategy (PARS)",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1647521,
       "holdingId":0,
       "fundName":"PIMCO Global Credit Opportunity Onshore Fund LLC",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1476751,
       "holdingId":0,
       "fundName":"Pleiad Asia Master Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1278023,
       "holdingId":0,
       "fundName":"Portland Hill",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1264461,
       "holdingId":0,
       "fundName":"Prince Street International Ltd.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1899361,
       "holdingId":19138335,
       "fundName":"Prospect Ridge U.S. RE Fund III",
       "assetClass":"Real Estate"
    },
    { 
       "fundId":1546073,
       "holdingId":0,
       "fundName":"Prudential Emerging Markets Debt Long/Short",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151217,
       "holdingId":0,
       "fundName":"PSAM WorldArb Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1766529,
       "holdingId":0,
       "fundName":"PSquared US Feeder Fund, LP",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1394931,
       "holdingId":0,
       "fundName":"QMS Diversified Global Macro",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151227,
       "holdingId":0,
       "fundName":"Quantitative Global Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1403037,
       "holdingId":0,
       "fundName":"Red Cliff Asia Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151237,
       "holdingId":0,
       "fundName":"Redmile Capital Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135303,
       "holdingId":0,
       "fundName":"Redwood Offshore Fund, Ltd.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1898885,
       "holdingId":0,
       "fundName":"Redwood Opportunity Domestic Fund, L.P.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1280043,
       "holdingId":0,
       "fundName":"Reef Road Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1681655,
       "holdingId":0,
       "fundName":"Renaissance Institutional Diversified Global Equities Onshore Fund L.P.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151241,
       "holdingId":0,
       "fundName":"Renaissance Institutional Equities Fund LLC",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151245,
       "holdingId":0,
       "fundName":"RG Niederhoffer Diversified Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1267279,
       "holdingId":0,
       "fundName":"Rhicon Strategic Program",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151247,
       "holdingId":0,
       "fundName":"Rimrock High Income Plus Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151249,
       "holdingId":0,
       "fundName":"Riva Ridge",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1317159,
       "holdingId":18723585,
       "fundName":"Riverstone Global Energy & Power Fund V",
       "assetClass":"Real Assets"
    },
    { 
       "fundId":1687563,
       "holdingId":0,
       "fundName":"Rivulet Capital",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1679775,
       "holdingId":18954275,
       "fundName":"RLH IV",
       "assetClass":"Private Equity"
    },
    { 
       "fundId":1208799,
       "holdingId":0,
       "fundName":"Rockhampton Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1666837,
       "holdingId":0,
       "fundName":"Rokos Global Macro Fund LP",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1421649,
       "holdingId":0,
       "fundName":"ROW Diversified Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1639217,
       "holdingId":0,
       "fundName":"RTW Onshore Fund One, L.P.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1255055,
       "holdingId":0,
       "fundName":"RWC European Focus Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1301413,
       "holdingId":0,
       "fundName":"Sachem Head",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1308265,
       "holdingId":0,
       "fundName":"Saemor Europe Alpha Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135317,
       "holdingId":0,
       "fundName":"Samlyn Offshore, Ltd.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135319,
       "holdingId":0,
       "fundName":"Samlyn Onshore Fund, L.P.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1845553,
       "holdingId":0,
       "fundName":"Sand Grove Opportunities Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1229439,
       "holdingId":0,
       "fundName":"Sarissa Capital",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1675671,
       "holdingId":0,
       "fundName":"Scopus Partners LP",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1349463,
       "holdingId":0,
       "fundName":"Sculptor Credit Opportunities Domestic Partners, L.P.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135273,
       "holdingId":0,
       "fundName":"Sculptor Domestic Partners II, L.P.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135275,
       "holdingId":0,
       "fundName":"Sculptor Overseas Fund II, Ltd.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1741189,
       "holdingId":0,
       "fundName":"SECOR Alpha Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1408245,
       "holdingId":0,
       "fundName":"Sector Healthcare Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1853379,
       "holdingId":0,
       "fundName":"Securis Opportunities Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151273,
       "holdingId":0,
       "fundName":"SEG Partners Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1556179,
       "holdingId":0,
       "fundName":"Segantii Asia-Pacific Equity Multi-Strategy Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135321,
       "holdingId":0,
       "fundName":"Senator Global Opportunity Fund LP",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1674983,
       "holdingId":0,
       "fundName":"Serengeti Lycaon Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151287,
       "holdingId":0,
       "fundName":"Serica Credit Balanced Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1546235,
       "holdingId":18885993,
       "fundName":"Shamrock Capital Content Fund I, L.P.",
       "assetClass":"Private Equity"
    },
    { 
       "fundId":1348439,
       "holdingId":18888185,
       "fundName":"Shamrock Capital Growth Fund IV",
       "assetClass":"Private Equity"
    },
    { 
       "fundId":1483377,
       "holdingId":0,
       "fundName":"Shellback Capital, LP",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1787701,
       "holdingId":0,
       "fundName":"Shelter Growth Opportunities Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1152267,
       "holdingId":0,
       "fundName":"Shepherd Investments International, Ltd.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135327,
       "holdingId":0,
       "fundName":"Silver Point Capital Offshore Fund, Ltd.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1810915,
       "holdingId":19077743,
       "fundName":"Silver Point Distressed Opportunity Institutional Partners, L.P.",
       "assetClass":"Private Equity"
    },
    { 
       "fundId":1666839,
       "holdingId":0,
       "fundName":"Silver Ridge Macro Master Fund Ltd.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1680169,
       "holdingId":0,
       "fundName":"Silver Rock Opportunistic Credit Fund LP",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1438271,
       "holdingId":0,
       "fundName":"SIR Hedged Equity Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135329,
       "holdingId":0,
       "fundName":"Sirios Capital Partners, L.P",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1726753,
       "holdingId":18988941,
       "fundName":"Siris Partners IV, L.P.",
       "assetClass":"Private Equity"
    },
    { 
       "fundId":1688233,
       "holdingId":0,
       "fundName":"Snow Lake China Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1166569,
       "holdingId":0,
       "fundName":"Sola I",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1183693,
       "holdingId":0,
       "fundName":"Sound Point Credit Opportunities Fund, L.P.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135519,
       "holdingId":0,
       "fundName":"Southpaw Credit Opportunity Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135521,
       "holdingId":0,
       "fundName":"Southpoint Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1161399,
       "holdingId":0,
       "fundName":"Spinnaker Global Emerging Markets Fund Ltd.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1388799,
       "holdingId":0,
       "fundName":"SRS Partners, Ltd",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1781229,
       "holdingId":0,
       "fundName":"SSI Hedged Convertible Opportunity Strategy",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135523,
       "holdingId":0,
       "fundName":"Standard General",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1267183,
       "holdingId":0,
       "fundName":"Standard Life Investments GARS",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1229479,
       "holdingId":0,
       "fundName":"Starboard Value and Opportunity Fund LP",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1942955,
       "holdingId":0,
       "fundName":"Stieven Financial Investors, LP",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1157591,
       "holdingId":0,
       "fundName":"Stone Milliner Macro Fund Inc.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151305,
       "holdingId":0,
       "fundName":"Strategic Value Restructuring Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1299885,
       "holdingId":0,
       "fundName":"Suvretta Partners, LP",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151743,
       "holdingId":0,
       "fundName":"Swiftcurrent Offshore Ltd",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1670605,
       "holdingId":0,
       "fundName":"Sylebra Bell Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151313,
       "holdingId":0,
       "fundName":"Symphony Long-Short Credit Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1670657,
       "holdingId":0,
       "fundName":"Synergy Fund Limited",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1670659,
       "holdingId":0,
       "fundName":"Systematica Alternative Markets Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1942455,
       "holdingId":0,
       "fundName":"Systematica Alternative Risk Premia",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135115,
       "holdingId":0,
       "fundName":"Systematica BlueTrend Fund Limited",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135333,
       "holdingId":0,
       "fundName":"Taconic Opportunity Fund L.P.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1392907,
       "holdingId":0,
       "fundName":"Taiyo Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151319,
       "holdingId":0,
       "fundName":"TCM Asia Opportunities Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151321,
       "holdingId":0,
       "fundName":"Tenor Opportunity Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1249809,
       "holdingId":0,
       "fundName":"Tensile Capital Partners",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1242465,
       "holdingId":0,
       "fundName":"The Adelphi Europe Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1312255,
       "holdingId":0,
       "fundName":"The Cassiopeia Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151323,
       "holdingId":0,
       "fundName":"The Childrens Investment Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1741799,
       "holdingId":0,
       "fundName":"The Colombard Fund, LP",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135339,
       "holdingId":0,
       "fundName":"Third Point Partners Qualified L.P.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1592679,
       "holdingId":18888053,
       "fundName":"Thompson Street Capital Partners IV, L.P.",
       "assetClass":"Private Equity"
    },
    { 
       "fundId":1735217,
       "holdingId":19025207,
       "fundName":"Thompson Street Capital Partners V, L.P.",
       "assetClass":"Private Equity"
    },
    { 
       "fundId":1409839,
       "holdingId":0,
       "fundName":"TIG Arbitrage Ehanced Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1315463,
       "holdingId":0,
       "fundName":"Tiger Eye Partners, LP",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151327,
       "holdingId":0,
       "fundName":"Tiger Global L.P.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1965749,
       "holdingId":0,
       "fundName":"Tiger Global, Ltd.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1393173,
       "holdingId":0,
       "fundName":"Tilden Park Investment Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1401963,
       "holdingId":0,
       "fundName":"TOR Asia Credit Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1474685,
       "holdingId":0,
       "fundName":"Tosca",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151335,
       "holdingId":0,
       "fundName":"TPG-Axon Partners",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1781125,
       "holdingId":0,
       "fundName":"TPRV Capital Fund, LP",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151337,
       "holdingId":0,
       "fundName":"Transtrend Diversified Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1267099,
       "holdingId":0,
       "fundName":"Trend",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135527,
       "holdingId":0,
       "fundName":"Trian Partners",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1302021,
       "holdingId":0,
       "fundName":"Trivest Asset Limited China Focus Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1267191,
       "holdingId":0,
       "fundName":"Tse Capital Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1349015,
       "holdingId":18885923,
       "fundName":"TSG7 A L.P.",
       "assetClass":"Private Equity"
    },
    { 
       "fundId":1636387,
       "holdingId":18885925,
       "fundName":"TSG7 B L.P.",
       "assetClass":"Private Equity"
    },
    { 
       "fundId":1770775,
       "holdingId":19054593,
       "fundName":"TSG8 L.P.",
       "assetClass":"Private Equity"
    },
    { 
       "fundId":1151345,
       "holdingId":0,
       "fundName":"Tudor BVI Global Portfolio",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1685781,
       "holdingId":0,
       "fundName":"Tudor Riverbend Crossing Partners",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151349,
       "holdingId":0,
       "fundName":"Turiya Fund LP",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1663937,
       "holdingId":0,
       "fundName":"Two Sigma Absolute Return Enhanced Fund, LP",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1663123,
       "holdingId":0,
       "fundName":"Two Sigma Absolute Return Fund, LP",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1679961,
       "holdingId":0,
       "fundName":"Two Sigma Risk Premia Cayman Fund, Ltd.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1687419,
       "holdingId":0,
       "fundName":"Two Sigma Risk Premia Enhanced Fund, LP",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151353,
       "holdingId":0,
       "fundName":"Two Sigma Spectrum U.S. Fund, LP",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1264417,
       "holdingId":0,
       "fundName":"Tybourne Capital Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1942715,
       "holdingId":0,
       "fundName":"Unigestion Alternative Risk Premia",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1256937,
       "holdingId":0,
       "fundName":"Valiant Capital Partners",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135351,
       "holdingId":0,
       "fundName":"ValueAct Capital Partners II, L.P.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1173419,
       "holdingId":0,
       "fundName":"Varadero",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1304915,
       "holdingId":0,
       "fundName":"Varde Investment Partners",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151359,
       "holdingId":0,
       "fundName":"Venor Capital Partners",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135353,
       "holdingId":0,
       "fundName":"Viking Global Equities III, Ltd",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1657597,
       "holdingId":18932623,
       "fundName":"Virgo Societas Partnership IV",
       "assetClass":"Private Equity"
    },
    { 
       "fundId":1659457,
       "holdingId":0,
       "fundName":"Vista Public Strategies Fund, LP",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1476711,
       "holdingId":0,
       "fundName":"Voleon Global Strategy Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1439223,
       "holdingId":18867837,
       "fundName":"VPC Special Opportunities Fund III",
       "assetClass":"Private Equity"
    },
    { 
       "fundId":1158079,
       "holdingId":0,
       "fundName":"VR Global Offshore Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1675059,
       "holdingId":0,
       "fundName":"Waterfall Eden Fund, L.P.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151367,
       "holdingId":0,
       "fundName":"Weiss Multi-Strategy Partners",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1741185,
       "holdingId":0,
       "fundName":"Wellington ALTA",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1696231,
       "holdingId":0,
       "fundName":"Wellington Global Equity Absolute Return Partners, L.P.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151369,
       "holdingId":0,
       "fundName":"Welton Global Directional Portfolio",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1267189,
       "holdingId":0,
       "fundName":"Western Asset Macro Opportunities",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135361,
       "holdingId":0,
       "fundName":"Wexford Spectrum Fund, LP",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135363,
       "holdingId":0,
       "fundName":"Wexford Spectrum Offshore Fund, Ltd.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1658663,
       "holdingId":0,
       "fundName":"Whale Rock Flagship Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1275707,
       "holdingId":0,
       "fundName":"Whitebox Multi-Strategy Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151377,
       "holdingId":0,
       "fundName":"Whitebox Relative Value Partners",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135365,
       "holdingId":0,
       "fundName":"Winton Fund Limited",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151379,
       "holdingId":0,
       "fundName":"Wolverine Flagship Fund Trading Limited",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1267095,
       "holdingId":0,
       "fundName":"wPraxis Enhanced",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135531,
       "holdingId":0,
       "fundName":"Yannix Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151385,
       "holdingId":0,
       "fundName":"York Asian Opportunities",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135533,
       "holdingId":0,
       "fundName":"York Credit Opportunities Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135369,
       "holdingId":0,
       "fundName":"York European Opportunities Fund, LP",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1168729,
       "holdingId":0,
       "fundName":"York Global Credit Income Fund, LP",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1135375,
       "holdingId":0,
       "fundName":"York Investments, Ltd.",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151677,
       "holdingId":0,
       "fundName":"Zebedee Focus Fund",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1151389,
       "holdingId":0,
       "fundName":"Zebra Global Equity Fund, LP – Beta Neutral",
       "assetClass":"Hedge Funds"
    },
    { 
       "fundId":1549499,
       "holdingId":0,
       "fundName":"ZP Utility Fund",
       "assetClass":"Hedge Funds"
    }
 ]