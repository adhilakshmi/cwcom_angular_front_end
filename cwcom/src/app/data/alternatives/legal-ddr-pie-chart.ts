export const legalDDROuterChartData = [
    {
        name: 'Management & Incentive Fees',
        y: 6.25,
        color: '#336799'
    },
    {
        name: 'Hurdle Rates / Preferred Return',
        y: 6.25,
        color: '#86A7D2'
    },
    {
        name: 'GP Contribution',
        y: 6.25,
        color: '#DCE6F2'
    },
    {
        name: 'Expenses & Offsets',
        y: 6.25,
        color: '#C7D9F0'
    },
    {
        name: 'Lock-ups',
        y: 8.3,
        color: '#BFBFBF'
    },
    {
        name: 'Distributions',
        y: 8.3,
        color: '#4D4D4D'
    },
    {
        name: 'Gates & Suspensions',
        y: 8.3,
        color: '#969696'
    },
    {
        name: 'Investment Limitations',
        y: 6.25,
        color: '#BACCE3'
    },
    {
        name: 'Key Person Provisions',
        y: 6.25,
        color: '#95B3D7'
    },
    {
        name: 'No Fault Provisions',
        y: 6.25,
        color: '#204977'
    },
    {
        name: 'Side Pocket Limits',
        y: 6.25,
        color: '#021F61'
    },
    {
        name: 'Audits & Reporting',
        y: 6.25,
        color: '#C0C0C0'
    },
    {
        name: 'Valuation Methodology',
        y: 6.25,
        color: '#DDDDDD'
    },
    {
        name: 'Amendments & Consents',
        y: 6.25,
        color: '#EAEAEA'
    },
    {
        name: 'Advisory Boards',
        y: 6.25,
        color: '#F8F8F8'
    }
];

export const legalDDRInnerChartData = [
    {
        name: "Alignment of Interests",
        y: 25,
        color: '#95B3D9'
    },
    {
        name: "Liquidity Provisions",
        y: 25,
        color: '#A6A6A6'
    },
    {
        name: "Governance",
        y: 25,
        color: '#366092'
    },
    {
        name: "Monitoring",
        y: 25,
        color: '#7F7F7F'
    }
];