import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MultiAssetComponent } from './multi-asset.component';
import { ReactiveFormsModule } from '@angular/forms';

describe('MultiAssetComponent', () => {
  let component: MultiAssetComponent;
  let fixture: ComponentFixture<MultiAssetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule
      ],
      declarations: [ MultiAssetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MultiAssetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
