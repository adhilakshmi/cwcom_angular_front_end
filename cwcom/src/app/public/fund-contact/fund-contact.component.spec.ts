import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FundContactComponent } from './fund-contact.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MatTabsModule } from '@angular/material';

describe('FundContactComponent', () => {
  let component: FundContactComponent;
  let fixture: ComponentFixture<FundContactComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        MatTabsModule
      ],
      declarations: [ FundContactComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FundContactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
