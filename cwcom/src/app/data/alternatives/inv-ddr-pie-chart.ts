export const invDDROuterChartData : any[]  = [
    {
        name: 'Organizational Growth & Stability',
        y: 8.3,
        color: '#5080BD'
    },
    {
        name: 'Succession',
        y: 8.3,
        color: '#8FB3E3'
    },
    {
        name: 'Team Depth',
        y: 8.3,
        color: '#C7D9F1'
    },
    {
        name: 'Idea Generation',
        y: 8.3,
        color: '#B9CDE5'
    },
    {
        name: 'Portfolio Strategy',
        y: 8.3,
        color: '#95B3D7'
    },
    {
        name: 'Risk Management',
        y: 8.3,
        color: '#012060'
    },
    {
        name: 'Transparency',
        y: 8.3,
        color: '#404040'
    },
    {
        name: 'Alignment',
        y: 8.3,
        color: '#595959'
    },
    {
        name: 'Fees & Expenses',
        y: 8.3,
        color: '#7F7F7F'
    },
    {
        name: 'Market and Peer Benchmarks',
        y: 8.3,
        color: '#BFBFBF'
    },
    {
        name: 'Attribution',
        y: 8.3,
        color: '#D9D9D9'
    },
    {
        name: 'Length and Depth of Track Record',
        y: 8.3,
        color: '#F2F2F2'
    }
];

export const invDDRInnerChartData = [
    {
        name: "Team & Organization",
        y: 25,
        color: '#95B3D7'
    },
    {
        name: "Investment Process",
        y: 25,
        color: '#366092'
    },
    {
        name: "Governance",
        y: 25,
        color: '#A6A6A6'
    },
    {
        name: "Performance",
        y: 25,
        color: '#7F7F7F'
    }
];