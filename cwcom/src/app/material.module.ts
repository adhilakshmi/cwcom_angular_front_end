import { NgModule } from '@angular/core';

import {
  MatTabsModule,
  MatDialogModule,
  MatInputModule,
  MatAutocompleteModule
} from '@angular/material';

@NgModule({
  imports: [
    MatTabsModule,
    MatDialogModule,
    MatInputModule,
    MatAutocompleteModule
  ],
  exports: [
    MatTabsModule,
    MatDialogModule,
    MatInputModule,
    MatAutocompleteModule
  ]
})
export class MaterialModule {}